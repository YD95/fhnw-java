package inversmodel.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import util.Observable;
import util.Observer;
import util.TraceV7;

import javax.swing.JPanel;

import inversmodel.model.Model;
import util.MyBorderFactory;

//Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
//Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
//Name: 
//Vorname:

public class View extends JPanel implements Observer {
	// 14
	private TraceV7 trace = new TraceV7(this);
	private static final long serialVersionUID = 1L;
	private BDGPanel bdgPanel = new BDGPanel("BDGInv.png");
	private ReiterPanel reiterPanel = new ReiterPanel();
	private Model model = new Model();
	private Controller controller = new Controller(model);
	public ParameterPanel parameterPanel;

	/**
	 * Baut die View des GUIs ...
	 * 
	 * <pre>
	 * - Baut das GUI gem�ss Aufgabenstellung.
	 * - Ruft setView() des Controllers auf.
	 * - Registriert sich beim Model als Observer.
	 * - Ruft setParameter des Controllers auf.
	 * </pre>
	 * 
	 * @param controller Referenz des Controllers.
	 */
	public View() {
		trace.constructorCall();
		// 12
		parameterPanel = new ParameterPanel(controller);
	}

	/**
	 * Entsprechende Panel zu Aufdatieren aufrufen ...
	 * 
	 * <pre>
	 * - Entsprechendes update() des ReiterPanels aufrufen.
	 * </pre>
	 */
	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		// 1
	}

	/**
	 * SignalQuelle des Models starten
	 */
	public void startSignalQuelle() {
		trace.methodeCall();
		// 1
	}
}
