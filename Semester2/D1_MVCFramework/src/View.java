import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class View extends JPanel implements Observer, ActionListener {
	public JTextField tf1 = new JTextField(40), tf2 = new JTextField(40);
	private Controller controller;
	private JButton btOK = new JButton("OK");

	public View(Controller controller) {
		this.controller = controller;
		setLayout(new BorderLayout());
		JPanel pNord = new JPanel(new GridLayout(2, 2));
		pNord.add(new Label("Eingabetext"));
		pNord.add(new Label("Ausgabetext"));
		pNord.add(tf1);
		pNord.add(tf2);
		add(pNord, BorderLayout.NORTH);
		add(btOK, BorderLayout.SOUTH);
		btOK.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		controller.btAction(tf1.getText());
	}

	public void update(Observable obs, Object obj) {
		Model model = (Model) obs;
		String textInModel = model.getData();
		tf2.setText(textInModel);
	}
}
