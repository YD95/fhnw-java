import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MVCFramework extends JFrame {

	public MVCFramework() {
		Model model = new Model();
		Controller controller = new Controller(model);
		View view = new View(controller);
		controller.setView(view);
		model.addObserver((Observer) view);
		add(view);
		pack();
		setPreferredSize(getMinimumSize());
	}

	public static void main(String args[]) {
		MVCFramework demo = new MVCFramework();
		demo.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		demo.setVisible(true);
		demo.setTitle("MVCFramework");
	}
}
