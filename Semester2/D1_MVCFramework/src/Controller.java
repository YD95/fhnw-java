public class Controller {
	private Model model;
	private View view;

	public Controller(Model model) {
		this.model = model;
	}

	public void btAction(String stInfo) {
		model.setData(stInfo);
		view.tf1.setText("");
		view.tf1.requestFocus();
	}

	public void setView(View view) {
		this.view = view;
	}
}
