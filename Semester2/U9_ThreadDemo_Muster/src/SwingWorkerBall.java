import java.awt.Color;
import java.awt.Graphics;

import javax.swing.SwingWorker;

public class SwingWorkerBall extends SwingWorker {
	private boolean weitermachen = true;
	private Graphics g;
	private int x = 150, dx = 7;
	private int y = 150, dy = 2;
	private int d = 10;
	private int xLinks = 150, xRechts = 250;
	private int yOben = 150, yUnten = 250;
	private Color color = Color.red;
	private int ballNumber;

	public SwingWorkerBall(Graphics g, int ballNumber) {
		this.g = g;
		this.ballNumber = ballNumber;
		color = new Color((int) (Math.random() * 255),
				(int) (Math.random() * 255), (int) (Math.random() * 255));
	}

	public void stoppen() {
		weitermachen = false;
	}

	protected Object doInBackground() throws Exception {
		System.out.println("Ball: "+ballNumber+" started");
		while (weitermachen) {
			g.setColor(Color.black);
			g.drawRect(xLinks, yOben, xRechts - xLinks + d, yUnten - yOben + d);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				System.err.println("Ausnahme bei Sleep");
			}
			g.setColor(Color.white);
			g.fillOval(x, y, d, d);
			if (x + dx <= xLinks)
				dx = -dx;
			if (x + dx >= xRechts)
				dx = -dx;
			if (y + dy <= yOben)
				dy = -dy;
			if (y + dy >= yUnten)
				dy = -dy;
			x = x + dx;
			y = y + dy;
			g.setColor(color);
			g.fillOval(x, y, d, d);
		}
		g.setColor(Color.white);
		g.fillOval(x, y, d, d);
		System.out.println("Ball: "+ballNumber+" stopped");
		return null;
	}

}

// 1) SwingWorker is created as bridge betweens Java Essentials Classes and
// Swing by implements Future, and quite guarantee that outoput from methods
// process, publish and done will be on EventDispashThread
//
// 2) you can invoke SwingWorker from Executor, this is most safiest methods how
// to create multithreading in Swing by implements SwingWorker
//
// 3) notice carefully with number or thread, because Executor doesn't care
// somehow about SwingWorkers life_cycle
//
// 4) another important notice and here
//
// 5) for listening states from SwingWorker you have to implements
// PropertyChangeListener

