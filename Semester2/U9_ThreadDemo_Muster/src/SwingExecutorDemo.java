import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class SwingExecutorDemo extends JFrame implements ActionListener, WindowListener, PropertyChangeListener {
	private static final long serialVersionUID = 1L;
	private Button start, stop;
	private SwingWorkerBall[] ball = new SwingWorkerBall[50];
	private RunnableBall[] rball = new RunnableBall[50];
	int i = 0;
	ExecutorService threadExecutor = Executors.newFixedThreadPool(2);

	public SwingExecutorDemo() {
		setLayout(new FlowLayout());
		start = new Button("Start");
		add(start);
		start.addActionListener(this);
		stop = new Button("Stop");
		add(stop);
		stop.addActionListener(this);
		this.addWindowListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) {
			for (int i = 0; i < ball.length; i++) {
				Graphics g = getGraphics();
				ball[i] = new SwingWorkerBall(g, i);
				threadExecutor.execute(ball[i]);
//				rball[i] = new RunnableBall(getGraphics());
//				threadExecutor.execute(rball[i]);
				
//				ball[i].addPropertyChangeListener(this);
			}
		}
		if (e.getSource() == stop) {
			if (i == 0)
				return;
			i--;
			rball[i].stoppen();
			System.out.println("Stop");
		}
	}

	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}
				SwingExecutorDemo f = new SwingExecutorDemo();
				f.setSize(400, 300);
				f.setVisible(true);
				f.setBackground(Color.white);
			}
		});
	}

	public void propertyChange(PropertyChangeEvent p) {
		System.out.println("PropChanged" + p);
	}
}
