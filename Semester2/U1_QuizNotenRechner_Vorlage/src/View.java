import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import util.Observable;
import util.Observer;
import util.TraceV3;

public class View extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	TraceV3 trace = new TraceV3(this);
	public SmileyPanel smileyPanel = new SmileyPanel();
	public InputPanel inputPanel;
	public ButtonPanel buttonPanel;

	public View(Controller controller) {
		super(new GridBagLayout());
		trace.constructorCall();

		inputPanel = new InputPanel(controller);
		buttonPanel = new ButtonPanel(controller);

		add(inputPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 10, 10));

		add(smileyPanel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 10, 10));

		add(buttonPanel, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 10, 10));
	}

	public void update(Observable obs, Object obj) {
		trace.methodeCall();
	}
}
