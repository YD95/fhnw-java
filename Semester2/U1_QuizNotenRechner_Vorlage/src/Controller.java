import util.TraceV3;

public class Controller {
	private TraceV3 trace = new TraceV3(this);
	private Model model;
	private View view;

	public Controller(Model model) {
		trace.constructorCall();
	}

	public void setView(View view) {
		trace.methodeCall();
	}

	public void btBerechne() {
		trace.methodeCall();
	}

	public void btReset() {
		trace.methodeCall();
	}
}
