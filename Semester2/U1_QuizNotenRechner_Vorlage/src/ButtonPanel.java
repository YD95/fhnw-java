import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import util.MyBorderFactory;
import util.TraceV3;

public class ButtonPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private TraceV3 trace = new TraceV3(this);
	private Controller controller;
	private JButton btBerechne = new JButton("Berechne");
	private JButton btReset = new JButton("Reset");

	public ButtonPanel(Controller controller) {
		super(null);
		trace.constructorCall();

		setBorder(MyBorderFactory.createMyBorder("ButtonPanel"));
		
		add(btBerechne,new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 10, 10));
		add(btReset,new GridBagConstraints(1, 0, 1, 1, 0.5, 0.5, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 10, 10));
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		trace.eventCall();
	}
}
