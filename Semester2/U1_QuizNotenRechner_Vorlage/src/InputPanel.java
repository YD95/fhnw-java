import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import util.MyBorderFactory;
import util.Observable;
import util.TraceV3;

public class InputPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private TraceV3 trace = new TraceV3(this);
	public JTextField tfAnzahlPunkte = new JTextField("8.0");
	public JTextField tfMaxPunkte = new JTextField("12.0");
	public JTextField tfNote = new JTextField(10);
	private Controller controller;

	public InputPanel(Controller controller) {
		super(new GridBagLayout());
		trace.constructorCall();

		setBorder(MyBorderFactory.createMyBorder("InputPanel"));

		add(new JLabel("Erreichte Punkzahl: "), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 10, 10));
		add(new JLabel("Maximal Punktzahl: "), new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 10, 10));
		add(new JLabel("Note: "), new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 10, 10));

		add(tfAnzahlPunkte, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 10, 10));
		add(tfMaxPunkte, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 10, 10));
		add(tfNote, new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 10, 10));

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		trace.eventCall();
	}

	public void update(Observable obs, Object obj) {
		trace.methodeCall();
	}
}
