import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import util.MyBorderFactory;
import util.Observable;
import util.SimpleTimer;
import util.SimpleTimerListener;
import util.TraceV3;
import util.Utility;

public class SmileyPanel extends JPanel implements SimpleTimerListener {
	private static final long serialVersionUID = 1L;
	private TraceV3 trace = new TraceV3(this);
	private Image imSmiley = Utility.loadResourceImage("smileys2.png");
	double x = 0.0, xNeu = 0.0;
	private boolean firstTime = true;

	public SmileyPanel() {
		super(null);
		trace.constructorCall();
		setPreferredSize(new Dimension(300, 300));

		setBorder(MyBorderFactory.createMyBorder("SmileyPanel"));
	}

	@Override
	public void paintComponent(Graphics g) {
		trace.paintCall();
		super.paintComponent(g);

		g.drawImage(imSmiley, (int) x, 10, null);
	}

	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		Model model = (Model) obs;
		double note = model.getData();
		repaint();
		x = -(note - 1) * 180 + 240;
	}

	@Override
	public void timerAction() {
		
	}
}
