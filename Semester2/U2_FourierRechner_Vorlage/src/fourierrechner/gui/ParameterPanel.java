package fourierrechner.gui;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fourierrechner.model.Controller;
import util.MyBorderFactory;
import util.TraceV4;

public class ParameterPanel extends JPanel implements ActionListener, ItemListener {
	private static final long serialVersionUID = 1L;
	private TraceV4 trace = new TraceV4(this);
	private Controller controller;
	
	public JTextField tfAmp = new JTextField("1.0");
	public JTextField tfFreq = new JTextField("1.0");
	public JTextField tfHarm = new JTextField("10");
	public JComboBox<String> cbForm = new JComboBox<String>();
	
	private String[] formShape = new String[]{"Rechteck", "Dreieck", "Saegezahn", "Trapez", "Pulse"};
	

	public ParameterPanel(Controller controller) {
		trace.constructorCall();
		setLayout(new GridBagLayout());
		setBorder(MyBorderFactory.createMyBorder(" SignalParameter "));
		
		this.controller = controller;
		
		for(int i = 0;i<formShape.length;i++) {
			cbForm.addItem(formShape[i]);	
		}
		
		add(new JLabel("Amplitude:"), new GridBagConstraints(0,0,1,1,0.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		add(tfAmp, new GridBagConstraints(1,0,1,1,1.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		
		add(new JLabel("Frequenz:"), new GridBagConstraints(0,1,1,1,0.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		add(tfFreq, new GridBagConstraints(1,1,1,1,1.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		
		add(new JLabel("Wellenform:"), new GridBagConstraints(0,2,1,1,0.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		add(cbForm, new GridBagConstraints(1,2,1,1,1.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		
		add(new JLabel("Anzahl Harmonische:"), new GridBagConstraints(0,3,1,1,0.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		add(tfHarm, new GridBagConstraints(1,3,1,1,1.0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.BOTH,
				new Insets(10,10,10,10),0,0));
		
		tfAmp.addActionListener(this);
		tfFreq.addActionListener(this);
		tfHarm.addActionListener(this);
		
		cbForm.addItemListener(this);

		
	}

	/**
	 * <pre>
	 * -Liest Information aus GUI aus.
	 * - Berechnet entsprechendes Signal.
	 * - Legt es in Signal ab.
	 * - löst repaint aus.
	 * </pre>
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		trace.eventCall();
		controller.btBerechne();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		trace.eventCall();
		controller.btBerechne();
	}
}
