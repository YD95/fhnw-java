package fourierrechner.gui;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import fourierrechner.model.Controller;
import util.MyBorderFactory;
import util.TraceV4;

public class ButtonPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private TraceV4 trace = new TraceV4(this);
	private Controller controller;
	public JButton btBerechne; 
	
	public ButtonPanel(Controller controller) {
		trace.constructorCall();
		this.controller = controller;
		setLayout(new BorderLayout());
		setBorder(MyBorderFactory.createMyBorder(" Button "));
	
		btBerechne = new JButton("Berechne");
		btBerechne.addActionListener(this);
		add(btBerechne);
	

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		trace.eventCall();
		controller.btBerechne();
	}
}
