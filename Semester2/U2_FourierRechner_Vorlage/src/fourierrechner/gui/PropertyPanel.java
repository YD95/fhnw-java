package fourierrechner.gui;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fourierrechner.model.Model;
import util.MyBorderFactory;
import util.Observable;
import util.TraceV4;

public class PropertyPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private TraceV4 trace = new TraceV4(this);
	
	public JTextField tfEff = new JTextField();
	public JTextField tfPeak = new JTextField();
	public JTextField tfCrest = new JTextField();


	public PropertyPanel() {
		trace.constructorCall();
		setLayout(new GridBagLayout());
		setBorder(MyBorderFactory.createMyBorder(" Signaleigenschaften "));
		
		add(new JLabel("Effektivwert:"), new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0,
				GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(
				10, 10, 10, 10), 0, 0));
		add(tfEff, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(
				10, 10, 10, 10), 0, 0));
		
		add(new JLabel("Spitzenwert:"), new GridBagConstraints(0, 1, 1, 1, 0.0, 1.0,
				GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(
				10, 10, 10, 10), 0, 0));
		add(tfPeak, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(
				10, 10, 10, 10), 0, 0));
		
		add(new JLabel("Crest- Faktor:"), new GridBagConstraints(0, 2, 1, 1, 0.0, 1.0,
				GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(
				10, 10, 10, 10), 0, 0));
		add(tfCrest, new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(
				10, 10, 10, 10), 0, 0));
		
	}

	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		Model model = (Model) obs;
		tfEff.setText(String.valueOf(model.getRMS()));
		tfPeak.setText(String.valueOf(model.getPeak()));
		tfCrest.setText(String.valueOf(model.getCrest()));
		repaint();
	}
}
