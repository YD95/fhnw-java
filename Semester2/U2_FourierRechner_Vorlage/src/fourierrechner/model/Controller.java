package fourierrechner.model;
import fourierrechner.gui.View;
import util.TraceV4;

public class Controller {
	private TraceV4 trace = new TraceV4(this);
	private Model model;
	private View view;

	public Controller(Model model) {
		trace.constructorCall();
		this.model = model;
	}

	public void setView(View view) {
		trace.methodeCall();
		this.view = view;
	}

	public void btBerechne() {
		trace.methodeCall();
		model.berechne(	Double.parseDouble(view.parameterPanel.tfAmp.getText()),
						Double.parseDouble(view.parameterPanel.tfFreq.getText()),
						Integer.parseInt(view.parameterPanel.tfHarm.getText()),
						String.valueOf(view.parameterPanel.cbForm.getSelectedIndex()),
						620);
		
		
	}
}// end class
