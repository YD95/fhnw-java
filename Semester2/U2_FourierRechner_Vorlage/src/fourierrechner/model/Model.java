package fourierrechner.model;
import util.Observable;
import util.TraceV4;

public class Model extends Observable {
	private TraceV4 trace = new TraceV4(this);
	private double[] signal;
	double rmsValue;
	double peakValue;
	double crestValue;

	public Model() {
		trace.constructorCall();
	}

	private double[] berechnePulse(double amp, double freq, int nHarm, int N) {
		trace.methodeCall();
		double[] x = new double[N];
		double alpha = Math.PI / 3.0;
		for (int n = 0; n < N; n++) {
			for (int k = 1; k < nHarm + 2; k++) {
				x[n] += ((4 * amp) / Math.PI) * Math.cos(alpha * (2 * k - 1))
						* Math.sin(2.0 * Math.PI * (2 * k - 1) * freq * n / N) / (2 * k - 1);
			}
		}
		signal = x;
		return x;
	}

	private double[] berechneTrapez(double amp, double freq, int nHarm, int N) {
		trace.methodeCall();
		double[] x = new double[N];
		double alpha = Math.PI / 3.0;
		for (int n = 0; n < N; n++) {
			for (int k = 1; k < nHarm + 2; k++) {
				x[n] += ((4 * amp) / Math.PI) * Math.sin(alpha * (2 * k - 1))
						* Math.sin(2.0 * Math.PI * (2 * k - 1) * freq * n / N) / Math.pow((2 * k - 1), 2.0);
			}
		}
		signal = x;
		return x;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Rechtecksignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneRechteck(double amp, double freq, int nHarm, int N) {
		trace.methodeCall();
		double[] x = new double[N];

		for (int n = 0; n < x.length; n++) {
			for (int k = 1; k < nHarm + 1; k++) {
				x[n] += (4 * amp / Math.PI) * Math.sin(2 * Math.PI * freq * (2 * k - 1) * n / (N - 1)) / (2 * k - 1);
			}
		}
		signal = x;
		return x;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Dreiecksignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneDreieck(double amp, double freq, int nHarm, int N) {
		trace.methodeCall();
		double[] x = new double[N];

		for (int n = 0; n < x.length; n++) {
			for (int k = 1; k < nHarm + 1; k++) {
				int m = 0;
				if (k % 2 == 0) {
					m = 1;
				} else {
					m = -1;
				}
				x[n] += m * ((8 * Math.PI) / Math.pow(Math.PI, 2.0) * amp / Math.PI)
						* Math.sin(2 * Math.PI * freq * (2 * k - 1) * n / N) / Math.pow((2 * k - 1), 2.0);
			}
		}
		signal = x;
		return x;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Sägezahnsignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneSaegezahn(double amp, double freq, int nHarm, int N) {
		trace.methodeCall();
		double[] x = new double[N];

		for (int n = 0; n < x.length; n++) {
			for (int k = 1; k < nHarm + 1; k++) {

				x[n] += (2 * amp / Math.PI) * Math.sin(2 * Math.PI * freq * k * n / N) / k * Math.pow(-1, k + 1);

			}
		}
		signal = x;
		return x;
	}

	public void berechne(double amp, double freq, int nHarm, String form, int N) {
		trace.methodeCall();
		
		switch (form) {
		case "0":
			signal = berechneRechteck(amp, freq, nHarm, N);
			break;
		case "1":
			signal = berechneDreieck(amp, freq, nHarm, N);
			break;
		case "2":
			signal = berechneSaegezahn(amp, freq, nHarm, N);
			break;
		case "3":
			signal = berechneTrapez(amp, freq, nHarm, N);
			break;
		case "4":
			signal = berechnePulse(amp, freq, nHarm, N);
			break;
		default:
			break;
		}
		notifyObservers();
	}
	
	public double getRMS() {
		trace.methodeCall();
		
		rmsValue = 0;
		int i;
		for(i = 0; i<signal.length; i++) {
			rmsValue += Math.pow(signal[i], 2);
			System.out.println("value signal[i]: " + signal[i] + "index i: " +i);
		}// end for
		
		rmsValue = rmsValue/(i+1);
		rmsValue = Math.sqrt(rmsValue);
		
		return rmsValue;
	}
	
	public double getPeak() {
		trace.methodeCall();
		
		peakValue = 0;
		for(int i = 0; i<signal.length; i++) {
			if(signal[i]>peakValue) {
				peakValue = signal[i];
			}// end if
		}// end for
		
		return peakValue;
	}
	

	public double getCrest() {
		crestValue = 0;
		
		crestValue = peakValue/rmsValue;
		return crestValue;
	}

	public double[] getSignal() {
		trace.methodeCall();
		return signal;
	}

	
	@Override
	public void notifyObservers() {
		trace.methodeCall();
		setChanged();
		super.notifyObservers();
	}
}