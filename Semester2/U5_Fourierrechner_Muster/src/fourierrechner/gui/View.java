package fourierrechner.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import util.Observable;
import util.Observer;

public class View extends JPanel implements Observer, ActionListener {
	private static final long serialVersionUID = 1L;
	private Controller controller;
	public ParameterPanel jpPar = new ParameterPanel();
	private SpektrumPanel jpSpek = new SpektrumPanel();
	private SignalPanel jpSig = new SignalPanel();
	private JButton btBerechnen = new JButton("Signal berechnen");

	/**
	 * Erzeugt das GUI mit ParameterPanel, SpektrumPanel, SignalPanel und Button
	 * btBerechnen im GridBagLayout.
	 */
	public View(Controller controller) {
		super(new GridBagLayout());
		this.controller = controller;

		add(jpPar, new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		add(jpSpek, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		add(jpSig, new GridBagConstraints(1, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));

		btBerechnen.addActionListener(this);
		add(btBerechnen, new GridBagConstraints(0, 2, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
	}

	/**
	 * Ruft entsprechende Methode im Controller auf.
	 */
	public void actionPerformed(ActionEvent e) {
		controller.btBerechnen();
	}

	@Override
	public void update(Observable obs, Object obj) {
		jpSig.update(obs, obj);
		jpSpek.update(obs, obj);
	}
}
