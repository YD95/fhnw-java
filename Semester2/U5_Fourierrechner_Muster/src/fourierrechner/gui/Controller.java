package fourierrechner.gui;

import fourierrechner.model.Model;
import fourierrechner.model.SignalGenerator;

public class Controller {
	private View view;
	private Model model;
	private SignalGenerator signalGenerator = new SignalGenerator();

	/**
	 * Setzt das Attribut model.
	 * 
	 * @param view
	 */
	public Controller(Model model) {
		this.model = model;
	}

	/**
	 *
	 * <pre>
	 * - Holt und konvertiert die Frequenz, Amplituden und Phasen: - Falls
	 * view.jpPar.tfAmp.getText() den Text "Rechteck" oder "Breieck" liefert, werden
	 * die Amplituden und Phasen mittels der statischen Methode Model.getKoeff()
	 * berechnet, - Sonst werden sie mittels stringToCoeff() aus den Texten
	 * konvertiert. - Setzt die zugehörigen Parameter im Model - Setzt die
	 * Wellenform des signalGenerators entsprechend des Signals im Model. - Setzt
	 * die Frequenz des signalGenerators entsprechend der Frequenz f. model und
	 * setzt Signal und Spektrum im GUI.
	 */
	public void btBerechnen() {
		double[] amplitude = null;
		double[] phase = null;
		double f = Double.parseDouble(view.jpPar.tfFreq.getText());

		try {
			if (view.jpPar.tfAmp.getText().trim().equalsIgnoreCase("rechteck")) {
				amplitude = Model.getKoeff(f, Model.RECHTECK)[0];
				phase = Model.getKoeff(f, Model.RECHTECK)[1];
			} else if (view.jpPar.tfAmp.getText().trim().toLowerCase().equals("dreieck")) {
				amplitude = Model.getKoeff(f, Model.DREIECK)[0];
				phase = Model.getKoeff(f, Model.DREIECK)[1];
			} else {
				amplitude = this.stringToCoeff(view.jpPar.tfAmp.getText());
				phase = this.stringToCoeff(view.jpPar.tfPhase.getText());
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {

		}

		model.setParameter(amplitude, phase, f);
		signalGenerator.setWaveForm(model.getSignal());
		signalGenerator.setFrequency(f);
	}

	/**
	 * Konvertiert die Ziechenkette s mit durch Leerzeichen getrennten Zahlen in
	 * einen double Array.
	 * 
	 * @param s
	 * @return
	 */
	private double[] stringToCoeff(String s) {
		String[] tokens = s.split("[, ]+");
		double[] z = new double[tokens.length];
		for (int i = 0; i < z.length; i++) {
			z[i] = Double.parseDouble(tokens[i]);
		}
		return z;
	}

	public void setView(View view) {
		this.view = view;
	}
}
