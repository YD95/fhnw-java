package fourierrechner.gui;

import fourierrechner.model.Model;
import fourierrechner.model.SignalGenerator;

public class Controller {
	private View view;
	private Model model;

	public Controller(Model model) {
		this.model = model;
	}

	public void btBerechnen() {
	}

	public void setView(View view) {
	}

	/**
	 * Konvertiert die Ziechenkette s mit durch Leerzeichen getrennten Zahlen in
	 * einen double Array.
	 * 
	 * @param s
	 * @return
	 */
	private double[] stringToCoeff(String s) {
		String[] tokens = s.split("[, ]+");
		double[] z = new double[tokens.length];
		for (int i = 0; i < z.length; i++) {
			z[i] = Double.parseDouble(tokens[i]);
		}
		return z;
	}
}
