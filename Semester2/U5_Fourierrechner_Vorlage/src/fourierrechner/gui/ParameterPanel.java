package fourierrechner.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;

import javax.swing.JPanel;
import javax.swing.JTextField;

import util.MyBorderFactory;

public class ParameterPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	/**
	 * Erzeugt ein Parameter Panel, als GridBagLayout organisiert, mit Labeln und
	 * Textfeldern fuer Amplitude, Phase und Grundfrequenz.
	 */
	public ParameterPanel() {
	}
}
