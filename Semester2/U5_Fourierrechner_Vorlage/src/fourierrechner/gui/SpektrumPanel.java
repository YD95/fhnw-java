package fourierrechner.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import util.MyBorderFactory;
import util.Observable;

public class SpektrumPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public SpektrumPanel() {
		super(new GridBagLayout());
		setPreferredSize(new Dimension(375, 350));
	}

	public void update(Observable obs, Object obj) {
	}
}
