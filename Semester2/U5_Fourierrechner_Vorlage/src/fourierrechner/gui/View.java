package fourierrechner.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import util.Observable;
import util.Observer;

public class View extends JPanel implements Observer, ActionListener {
	private static final long serialVersionUID = 1L;
	private Controller controller;
	
	public SpektrumPanel spektrumPanel = new SpektrumPanel();
	public ParameterPanel parameterPanel = new ParameterPanel();
	public SignalPanel signalPanel = new SignalPanel();

	public View(Controller controller) {
	}

	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void update(Observable obs, Object obj) {
	}
}
