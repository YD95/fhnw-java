package fourierrechner.model;

import util.Observable;

public class Model extends Observable {

	public static final int RECHTECK = 0, DREIECK = 1;
	private double[] signal, tAxis, fAxis;
	private double[] amplitude, phase;
	private int nPunkte;

	/**
	 * <pre>
	 * - Setzt das entsprechende Attribut
	 * - Erzeugt die tAxis mit ensprechender Anzahl Punkte und Werten zwischen 0.0 und 1.0.
	 * </pre>
	 * 
	 * @param amplitude
	 * @param phase
	 * @param f
	 * @param nPunkte
	 */
	public Model(int nPunkte) {
		this.nPunkte = nPunkte;
		tAxis = new double[nPunkte];
		for (int i = 0; i < tAxis.length; i++) {
			tAxis[i] = i * (1.0 / (tAxis.length - 1));
		}
	}

	/**
	 * <pre>
	 * - Setzt die entsprechenden Attribute.
	 * - Berechnet dT gem�ss Aufgabenstellung.
	 * - Berechnet gem�ss Aufgabenstellung das zur Fourierreihe zugeh�rige Signal.
	 * - Erzeugt die Frequenzachse fAxis aufgrund der Anzahl Amplituden. 
	 * - Notifiziert den Observer.
	 * </pre>
	 * 
	 * @param amplitude
	 * @param phase
	 * @param f
	 */
	public void setParameter(double[] amplitude, double[] phase, double f) {
		this.phase = phase;
		this.amplitude = amplitude;

		double dT = 1.0 / (f * (nPunkte - 1));
		signal = new double[nPunkte];
		for (int i = 0; i < signal.length; i++) {
			for (int k = 0; (k < amplitude.length) & ((k + 1) * f < 24e3); k++) {
				signal[i] += amplitude[k] * Math.cos(2.0 * Math.PI * (k + 1) * f * i * dT + Math.toRadians(phase[k]));
			}
		}

		fAxis = new double[amplitude.length];
		for (int i = 0; i < fAxis.length; i++) {
			fAxis[i] = (i + 1) * f;
		}

		notifyObservers();
	}

	/**
	 * Gibt zum Model zugehoeriges Signal signal zurueck.
	 * 
	 * @return
	 */
	public double[] getSignal() {
		return signal;
	}

	/**
	 * Gibt zum Model zugehoerige Zeitachse tAxis zurueck.
	 * 
	 * @return
	 */
	public double[] getTAxis() {
		return tAxis;
	}

	/**
	 * Gibt zum Model zugehoerige Frequenzachse fAxis zurueck.
	 * 
	 * @return
	 */
	public double[] getFAxis() {
		return fAxis;
	}

	/**
	 * <pre>
	 * - Erzeugt einen zwei-dim. double - Array der Gr�sse [2][(int) (24e3 / f)].
	 * - F�r den Fall (waveform):
	 *   - RECHTECK:
	 *     - Amplituden und Phasen der Fourrierreihe f�rs Rechtecksignal gem�ss Aufgabenstellung berechnen.
	 *   - DREIECK:
	 *     - Amplituden und Phasen der Fourrierreihe f�rs Dreiecksignal gem�ss Aufgabenstellung berechnen.
	 * </pre>
	 * 
	 * @return
	 */
	public static double[][] getKoeff(double f, int waveform) {
		double[][] res = new double[2][(int) (24e3 / f)];

		switch (waveform) {
		case RECHTECK:
			for (int i = 0; i < res[0].length; i += 2) {
				res[0][i] = 1.0 / (i + 1);
				res[1][i] = -90;
			}
			break;
		case DREIECK:
			double sign = 1.0;
			for (int i = 0; i < res[0].length; i += 2) {
				res[0][i] = sign / ((i + 1) * (i + 1));
				sign *= -1.0;
				res[1][i] = -90;
			}
			break;
		}
		return res;
	}

	public double[] getPhase() {
		return phase;
	}

	public double[] getAmplitude() {
		return amplitude;
	}

	@Override
	public void notifyObservers() {
		setChanged();
		super.notifyObservers();
	}
}
