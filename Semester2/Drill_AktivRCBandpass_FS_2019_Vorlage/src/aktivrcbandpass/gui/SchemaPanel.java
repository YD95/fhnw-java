// Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
// Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
// Name: 
// Vorname:
// Modulanlass:

package aktivrcbandpass.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import util.TraceV7;

public class SchemaPanel extends JPanel {
	private TraceV7 trace = new TraceV7(this);
	private static final long serialVersionUID = 1L;
	// 2 + 2

	
	/**
	 * Konstruktor der Klasse SchemaPanel.
	 * 
	 * <pre>
	 * - Baut das User-Interface gem�ss Angaben in der Aufgabenstellung.
	 * </pre>
	 * 
	 */
	public SchemaPanel() {
		super(new GridBagLayout());
		trace.constructorCall();
		// 2
	}
}
