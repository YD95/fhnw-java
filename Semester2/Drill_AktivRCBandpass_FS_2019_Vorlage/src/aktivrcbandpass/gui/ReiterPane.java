// Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
// Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
// Name: 
// Vorname:
// Modulanlass:

package aktivrcbandpass.gui;

import javax.swing.JTabbedPane;

import util.MyBorderFactory;
import util.Observable;
import util.TraceV7;

public class ReiterPane extends JTabbedPane {
	private TraceV7 trace = new TraceV7(this);
	private static final long serialVersionUID = 1L;
	// 6 + 3

	
	/**
	 * <pre>
	 * - Baut das User-Interface gem�ss Angaben in der Aufgabenstellung.
	 * </pre>
	 * 
	 */
	public ReiterPane() {
		trace.constructorCall();
		// 4
	}

	/**
	 * - Ruft update() der entsprechenden Panel auf.
	 */
	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		// 2
	}

}
