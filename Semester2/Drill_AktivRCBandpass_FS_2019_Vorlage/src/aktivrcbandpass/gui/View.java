// Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
// Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
// Name: 
// Vorname:
// Modulanlass:

package aktivrcbandpass.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import util.Observable;
import util.Observer;
import util.TraceV7;

public class View extends JPanel implements Observer {
	private TraceV7 trace = new TraceV7(this);
	private static final long serialVersionUID = 1L;
	// 5 + 2

	
	/**
	 * <pre>
	 * - Baut das User-Interface gem�ss Angaben in der Aufgabenstellung.
	 * </pre>
	 * 
	 * @param controller
	 */
	public View(Controller controller) {
		super(new GridBagLayout());
		trace.constructorCall();
		// 3

	}

	/**
	 * - Ruft update() der entsprechenden Componenten auf.
	 */
	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		// 2
	}
}
