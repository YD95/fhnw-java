// Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
// Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
// Name: 
// Vorname:
// Modulanlass:

package aktivrcbandpass.gui;

import aktivrcbandpass.model.Model;
import util.TraceV7;

public class Controller {
	private TraceV7 trace = new TraceV7(this);
	// 24
	private Model model;
	private View view;

	public Controller(Model model) {
		trace.constructorCall();
		this.model = model;
	}

	public void setView(View view) {
		trace.methodeCall();
		this.view = view;
	}

	/**
	 * <pre>
	 * Hinweis: Die gew�hlte Zeichenkette kann mittels xyzSpinner.getValue().toString() aus einem Spinner 
	 *          ausgelesen werden.
	 * 
	 * - Dieser Methode kommt die Aufgabe zu, entsprechend der Zeichenkette actionCommand ("Q", "F", "R", oder "C") 
	 *   die richtige Setter-Methode des Models, mit dem entsprechenden Wert aufzurufen. 
	 *   
	 * - Wenn das actionCommand ein "C" ist, muss je nach Prefix-Spinner Wahl, der Zahlenwert im C-Textfeld mit 1e-12 ... 1e-6 multipliziert werden, 
	 *   bevor die Setter-Methode des Models aufgerufen wird.
	 *   
	 * - Der Code kann Wahlweise mit Switch-Anweisungen oder mit if-Anweisungen programmiert werden.
	 *    
	 * - Ruft zum Schluss die Methode berechne() des Models auf.
	 * </pre>
	 * 
	 * @param actionCommand
	 */
	public void aktion(String actionCommand) {
		trace.methodeCall();
		// 14
	}
}
