// Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
// Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
// Name: 
// Vorname:
// Modulanlass:

package aktivrcbandpass.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import aktivrcbandpass.model.Model;
import util.MyBorderFactory;
import util.Observable;
import util.TraceV7;

public class InputPanel extends JPanel implements ActionListener, ChangeListener {
	private TraceV7 trace = new TraceV7(this);
	private static final long serialVersionUID = 1L;
	// 33
	private Controller controller;
	public JTextField tfF = new JTextField("10e3");
	public JTextField tfQ = new JTextField("10.0");
	public JTextField tfR = new JTextField("1.591549431E7");
	private String[] stE12 = new String[] { "1.0", "1.2", "1.5", "1.8", "2.2", "2.7", "3.3", "3.9", "4.7", "5.6", "6.8",
			"8.2" };
	public JSpinner valueSpinner = new JSpinner(new SpinnerListModel((stE12)));
	public JSpinner prefixSpinner = new JSpinner(new SpinnerListModel(new String[] { "pF", "nF", "\u03BCF" }));

	/**
	 * <pre>
	 * - Baut das User-Interface gem�ss Angaben in der Aufgabenstellung.
	 * - Setzt mittels tfF.setActionCommand("F") das ActionCommand des Textfeldes auf "F" 
	 *   und die restlichen ActionCommands der Textfelder entsprechend auf "Q" und "R".
	 * - Setzt entsprechendes Attribut.
	 * </pre>
	 * 
	 * @param controller
	 */
	public InputPanel(Controller controller) {
		super(new GridBagLayout());
		trace.constructorCall();
		// 21
	}

	/**
	 * - Expandiert die E-Reihe der Art, dass in einem neuen Zeichenketten-Array die
	 *   36 Werte von "1.0" bis "820.0" stehen:
	 * 
	 * <pre>
	 * - Erzeugt einen lokalen String-Array res der dreifachen L�nge von eReihe.
	 * - Deklariert einen lokalen double-Wert faktor gleich 1.
	 * - F�r k gleich Null bis kleiner 3:
	 *   - F�r i = Null bis kleiner der L�nge der eReihe: 
	 *     - res and der Stelle (i plus k mal L�nge der eReihe) gleich dem, auf eine Stellen gerundeten Wert,
	 *       von eReihe an der Stelle i mal faktor, setzen.
	 *       Hinweis: res und eReihe sind Arrays mit Zeichenketten. Sie m�ssen entsprechend gewandelt werden.
	 *   - faktor gleich faktor mal 10.0 setzen.
	 * - res mittels return zur�ckgeben.
	 * </pre>
	 * 
	 * - Die Methode kann in der Folge direkt beim Erzeugen des Spinners verwendet werden.
	 * 
	 * @param eReihe
	 * @return
	 */
	private String[] expandReihe(String[] eReihe) {
		trace.methodeCall();
		// 7

		return null; // Um den Kompiler gl�cklich zu halten
	}

	/**
	 * 
	 * <pre>
	 * - Holt mittels getParameter() die Parameter aus dem Model und setzt die 
	 *   entsprechenden Werte gerundet in die TextFelder. Schauen Sie sich die 
	 *   Methode im Model an!
	 * </pre>
	 * 
	 * @param obs
	 * @param obj
	 */
	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		// 5
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		trace.eventCall();
		controller.aktion(e.getActionCommand());
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		trace.eventCall();
		controller.aktion("C");
	}
}
