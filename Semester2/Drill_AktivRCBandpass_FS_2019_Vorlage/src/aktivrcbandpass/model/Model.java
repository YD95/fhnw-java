// Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
// Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
// Name: 
// Vorname:
// Modulanlass:

package aktivrcbandpass.model;

import util.Observable;
import util.TraceV7;

public class Model extends Observable {
	private TraceV7 trace = new TraceV7(this);
	// 11

	public static int F = 0, Q = 1, R = 2, C = 3;
	private Complex[] H = new Complex[0];
	private double[] fAchse;
	// Default - Values
	private double q = 10.0;
	private double fr = 1e4;
	private double c = 1e-12;
	private double r = 1.591549431E7;

	/**
	 * Konstruktor der Klasse Model.
	 * 
	 * <pre>
	 * - Erzeugt mittels linspace() von PicoMatlab die Frequenzachse mit Bereich von 0 bis 20kHz und 1024 Punkten.
	 * </pre>
	 */
	public Model() {
		trace.constructorCall();
		// 1
	}

	/**
	 * <pre>
	 * - Setzt das entsprechende Attribut.
	 * - Berechnet die Resonanzfrequenz fr neu.
	 * </pre>
	 * 
	 * @param r
	 */
	public void setR(double r) {
		trace.methodeCall();
		// 2
	}

	/**
	 * <pre>
	 * - Setzt das entsprechende Attribut.
	 * - Berechnet den Wert des Widerstandes r neu.
	 * </pre>
	 * 
	 * @param fr
	 */
	public void setFr(double fr) {
		trace.methodeCall();
		// 2
	}

	/**
	 * <pre>
	 * - Setzt das entsprechende Attribut.
	 * - Berechnet den Wert des Widerstandes r neu.
	 * </pre>
	 * 
	 * @param c
	 */
	public void setC(double c) {
		trace.methodeCall();
		// 2
	}

	/**
	 * <pre>
	 * - Berechnet und baut den Array b mit den entsprechenden Werten.
	 * - Berechnet und baut den Array a mit den entsprechenden Werten.
	 * - Berechnet aufgrund von b, a und fAchse, mittels freqs() der Klasse PicoMatlab, den Frequenzgang H.
	 * - Notifiziert die Beobachter.
	 * </pre>
	 * 
	 */
	public void berechne() {
		trace.methodeCall();
		// 4
	}

	public void setQ(double q) {
		trace.methodeCall();
		this.q = q;
	}

	public double[] getFAchse() {
		trace.methodeCall();
		return fAchse;
	}

	public double[] getAmplitude() {
		trace.methodeCall();
		return Complex.abs(H);
	}

	public double[] getPhase() {
		trace.methodeCall();
		return Complex.angle(H);
	}

	public double[] getParameter() {
		trace.methodeCall();
		return new double[] { fr, q, r, c };
	}

	public void notifyObservers() {
		trace.methodeCall();
		setChanged();
		super.notifyObservers();
	}
}
