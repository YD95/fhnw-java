import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import util.Observer;
import util.TraceV4;

public class MVCFramework extends JFrame {
	private TraceV4 trace = new TraceV4(this);
	private static final long serialVersionUID = 1L;
	private Model model = new Model();
	private Controller controller = new Controller(model);
	private View view = new View(controller);

	public MVCFramework() {
		trace.constructorCall();
		controller.setView(view);
		model.addObserver((Observer) view);
		trace.registerObserver(model, view);
		add(view, BorderLayout.CENTER);
		TraceV4.eventScrollPane.setPreferredSize(new Dimension(-1, 100));
		add(TraceV4.eventScrollPane, BorderLayout.SOUTH);
		pack();
		setMinimumSize(getPreferredSize());
		setResizable(true);
	}

	public static void main(String args[]) {
		TraceV4.mainCall(true, true, true);
		MVCFramework demo = new MVCFramework();
		demo.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		demo.setVisible(true);
		demo.setTitle("MVCFramework");
	}
}
