import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import util.MyBorderFactory;
import util.TraceV4;

public class ButtonPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private TraceV4 trace = new TraceV4(this);
	private Controller controller;
	private JButton btBerechne = new JButton("Berechne");
	private JButton btReset = new JButton("Reset");

	public ButtonPanel(Controller controller) {
		super(new GridBagLayout());
		trace.constructorCall();
		this.controller = controller;
		setBorder(MyBorderFactory.createMyBorder(" ButtonPanel "));

		add(btBerechne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		btBerechne.addActionListener(this);

		add(btReset, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		btReset.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		trace.eventCall();
		if (e.getSource() == btBerechne) {
			controller.btBerechne();
		}
		if (e.getSource() == btReset) {
			controller.btReset();
		}
	}
}
