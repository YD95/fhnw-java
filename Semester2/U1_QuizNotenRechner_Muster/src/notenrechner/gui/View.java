package notenrechner.gui;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import util.Observable;
import util.Observer;
import util.TraceV4;

public class View extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	TraceV4 trace = new TraceV4(this);
	public InputPanel inputPanel;
	public SmileyPanel smileyPanel = new SmileyPanel();
	public ButtonPanel buttonPanel;

	public View(Controller controller) {
		super(new GridBagLayout());
		trace.constructorCall();

		inputPanel = new InputPanel(controller);
		buttonPanel = new ButtonPanel(controller);

		add(inputPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(10, 10, 10, 10), 0, 0));
		add(smileyPanel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(10, 10, 10, 10), 0, 0));
		add(buttonPanel, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));
	}

	@Override
	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		inputPanel.update(obs, obj);
		smileyPanel.update(obs, obj);
	}
}
