import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import util.MyBorderFactory;
import util.Observable;
import util.SimpleTimer;
import util.SimpleTimerListener;
import util.TraceV4;
import util.Utility;

public class SmileyPanel extends JPanel implements SimpleTimerListener {
	private static final long serialVersionUID = 1L;
	private TraceV4 trace = new TraceV4(this);
	private Image imSmiley = Utility.loadResourceImage("smileys2.png"); // 900 X 300 px
	double x = 0.0, xNeu = 0.0;

	public SmileyPanel() {
		super(null);
		trace.constructorCall();
		setPreferredSize(new Dimension(400, 300));
		setBorder(MyBorderFactory.createMyBorder(" SmileyPanel "));
		(new SimpleTimer(50, this)).start();
	}

	@Override
	public void paintComponent(Graphics g) {
//		trace.paintCall();
		super.paintComponent(g);
		g.drawImage(imSmiley, (int) x, 10, this);
		g.drawLine(200, 10, 200, 280);
	}

	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		Model model = (Model) obs;
		double note = model.getData();
		xNeu = (int) (-note * 180 + 470);
	}

	@Override
	public void timerAction() {
//		trace.eventCall();
		x = (x * 0.99 + 0.01 * xNeu);
		repaint();
	}
}
