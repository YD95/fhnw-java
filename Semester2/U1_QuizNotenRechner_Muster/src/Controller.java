import util.TraceV4;

public class Controller {
	private TraceV4 trace = new TraceV4(this);
	private Model model;
	private View view;

	public Controller(Model model) {
		trace.constructorCall();
		this.model = model;
	}

	public void setView(View view) {
		trace.methodeCall();
		this.view = view;
	}

	public void btBerechne() {
		trace.methodeCall();
		double anzahlPunkte = Double.parseDouble(view.inputPanel.tfAnzahlPunkte.getText());
		double maxPunkte = Double.parseDouble(view.inputPanel.tfMaxPunkte.getText());
		model.berechneNote(anzahlPunkte, maxPunkte);
	}

	public void btReset() {
		trace.methodeCall();
		view.inputPanel.tfAnzahlPunkte.setText("7.2");
		view.inputPanel.tfMaxPunkte.setText("12.0");
		btBerechne();
	}
}
