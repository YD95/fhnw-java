import util.Observable;
import util.TraceV4;

public class Model extends Observable {
	private TraceV4 trace = new TraceV4(this);
	private double note = 4.3;

	public Model() {
		trace.constructorCall();
	}

	public double getData() {
		trace.methodeCall();
		return note;
	}

	public void notifyObservers() {
		trace.methodeCall();
		setChanged();
		super.notifyObservers();
	}

	public void berechneNote(double anzahlPunkte, double maxPunkte) {
		trace.methodeCall();
		note = 5 * anzahlPunkte / maxPunkte + 1;
		notifyObservers();
	}
}