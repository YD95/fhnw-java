package taschenrechner;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;

import taschenrechner.gui.*;
import taschenrechner.model.*;
import util.*;


public class Taschenrechner extends JFrame {
	private static final long serialVersionUID = 1L;
	private TraceV5 trace = new TraceV5(this);

	public Taschenrechner() {
		trace.constructorCall();
		Model model = new Model();
		Controller controller = new Controller(model);
		View view = new View(controller);
		model.addObserver(view);
		trace.registerObserver(model, view);

		getContentPane().add(view);

		// Center the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
	}

	public static void main(String args[]) {
		TraceV5.mainCall(true, true, true);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				Taschenrechner frame = new Taschenrechner();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setTitle("RPN Calculator");
				frame.pack();
				frame.setMinimumSize(frame.getPreferredSize());
				frame.setVisible(true);
			}
		});
	}
}
