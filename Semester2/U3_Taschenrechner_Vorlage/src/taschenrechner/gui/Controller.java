package taschenrechner.gui;

import taschenrechner.model.*;
import util.*;

public class Controller {
	static final int ENTER = 0;
	static final int NUMBER = 1;
	static final int OPERATION = 2;

	private TraceV5 trace = new TraceV5(this);

	private int condition = ENTER;

	private Model model;

	public Controller(Model model) {
		trace.constructorCall();
		this.model = model;
	}

	/**
	 * <pre>
	 * - Ruft entsprechende Methode des models auf.
	 * - Setzt zustand auf OPERATION.
	 * </pre>
	 */
	public void add() {
		trace.methodeCall();
		model.add();
		condition = OPERATION;
	}

	/**
	 * <pre>
	 * - Ruft entsprechende Methode des models auf.
	 * - Setzt zustand auf OPERATION.
	 * </pre>
	 */
	public void subtract() {
		trace.methodeCall();
		model.subtract();
		condition = OPERATION;
	}

	/**
	 * <pre>
	 * - Ruft entsprechende Methode des models auf.
	 * - Setzt zustand auf OPERATION.
	 * </pre>
	 */
	public void multiply() {
		trace.methodeCall();
		model.multiply();
		condition = OPERATION;
	}

	/**
	 * <pre>
	 * - Ruft entsprechende Methode des models auf.
	 * - Setzt zustand auf OPERATION.
	 * </pre>
	 */
	public void divide() {
		trace.methodeCall();
		model.divide();
		condition = OPERATION;
	}

	/**
	 * <pre>
	 * - Ruft entsprechende Methode des models auf.
	 * - Setzt zustand auf ENTER.
	 * </pre>
	 */
	public void enter() {
		trace.methodeCall();
		model.enter();
		condition = ENTER;
	}

	/**
	 * <pre>
	 * Falls lastOp gleich ZAHL
	 * 		Value des Models mit 10 multiplizieren und neue Zahl i dazu addieren.
	 * 		Value des Models entsprechend setzen.
	 * Falls latsOP gleich OPERATION
	 * 		enter() des Objektes model aufrufen.
	 * 		Value des Models auf i setzen.
	 * Falls lastOp gleich ENTER
	 * 		Value des Models auf i setzen.
	 * </pre>
	 * 
	 * @param i
	 */
	public void number(int i) {
		trace.methodeCall();

		switch (condition) {
		case NUMBER:
			model.setValue(model.getValue() * 10 + i);
			break;
		case OPERATION:
			model.enter();
			model.setValue(i);
			break;
		case ENTER:
			model.setValue(i);
			break;
		}

		condition = NUMBER;
	}

}
