package taschenrechner.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import taschenrechner.model.*;
import util.*;

public class View extends JPanel implements Observer, ActionListener {
	TraceV5 trace = new TraceV5(this);
	private static final long serialVersionUID = 1L;
	private JButton btEnter = new JButton("Enter");
	private JTextField tfOut = new JTextField(30);
	private JPanel panelButton = new JPanel(new GridLayout(4, 4, 10, 10));
	private JButton[][] button = new JButton[4][4];
	private String[][] stButtonText = { { "7", "8", "9", "/" }, { "4", "5", "6", "*" }, { "1", "2", "3", "-" },
			{ "0", "", "", "+" } };
	private Controller controller;

	/**
	 * <pre>
	 * - Setzt BorderLayout(10,10); - Setzt Attribute controller. - Erzeugt die
	 * Button, f�gt sie dem Panel Button hinzu und registriert this als
	 * ActionListener bei jedem Button. - Baut den Rest des GUI.
	 * 
	 * </pre>
	 * 
	 * @param ctrl
	 */
	public View(Controller ctrl) {
		super(new BorderLayout(10, 10));
		trace.constructorCall();

		this.controller = ctrl;
		setBackground(Color.lightGray);
		for (int i = 0; i < button.length; i++) {
			for (int k = 0; k < button[0].length; k++) {
				button[i][k] = new JButton(stButtonText[i][k]);
				panelButton.add(button[i][k]);
				button[i][k].addActionListener(this);
			}
		}
		add(this.tfOut, BorderLayout.NORTH);
		panelButton.setBackground(Color.lightGray);
		add(panelButton, BorderLayout.CENTER);
		add(btEnter, BorderLayout.SOUTH);
		btEnter.addActionListener(this);
	}

//	/**
//	 * <pre>
//	 * - Schaut ob einer der Button 1 - 9 gedr�ckt wurde:
//	 * 	 - Falls ja:
//	 * 	 	 - number() von controller mit zugeh�riger Zahl aufrufen.
//	 * - Falls Button mit der 0:
//	 * 		 - number() von controller mit 0 aufrufen.
//	 * - Falls anderer Button:
//	 * 		 - entsprechende Methode von controller aufrufen.
//	 * 
//	 * </pre>
//	 */
//	public void actionPerformed(ActionEvent e) {
//		trace.eventCall();
//		for (int i = 0; i < 3; i++) {
//			for (int k = 0; k < 3; k++) {
//				if (button[i][k] == e.getSource()) {
//					controller.number(Integer.parseInt(((JButton) e.getSource()).getText()));
//				}
//			}
//		}
//		if (e.getSource() == button[3][0]) {
//			controller.number(0);
//		}
//		if (e.getSource() == button[3][3]) {
//			controller.add();
//		}
//		if (e.getSource() == button[2][3]) {
//			controller.subtract();
//		}
//		if (e.getSource() == button[1][3]) {
//			controller.multiply();
//		}
//		if (e.getSource() == button[0][3]) {
//			controller.divide();
//		}
//		if (e.getSource() == btEnter) {
//			controller.enter();
//		}
//	}

	/**
	 * <pre>
	 * - Checks which button was pressed and returns the text as integer back
	 * 	- run a try catch to handle the no number exceptions
	 * 
	 * </pre>
	 */
	public void actionPerformed(ActionEvent e) {
		trace.eventCall();

		try {

			controller.number(Integer.parseInt(((JButton) e.getSource()).getText()));

		} catch (Exception e2) {

			if (e.getSource() == button[3][3]) {
				controller.add();
			}
			if (e.getSource() == button[2][3]) {
				controller.subtract();
			}
			if (e.getSource() == button[1][3]) {
				controller.multiply();
			}
			if (e.getSource() == button[0][3]) {
				controller.divide();
			}
			if (e.getSource() == btEnter) {
				controller.enter();
			}
		}

	}

	/**
	 * <pre>
	 * - modelObject in Model wandeln.
	 * - Text in tfOut entsprechend Wert von getValue von model setzen.
	 * - StackInfo mittels syso anzeigen.
	 * </pre>
	 */
	public void update(Observable obs, Object obj) {
		trace.methodeCall();
		Model model = (Model) obs;
		tfOut.setText("" + model.getValue());
		System.out.println(model.getStackInfo());
	}
}
