import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class ExceptionDemo extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JTextField eingabeTf, ausgabeTf;
	private BufferedReader eingabeDatei;

	public ExceptionDemo() {
		setLayout(new GridBagLayout());

		add(new JLabel("  Geben Sie eine Ganzzahl ein:"), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		eingabeTf = new JTextField(25);
		eingabeTf.addActionListener(this);
		add(eingabeTf, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));

		add(new JLabel("  Ausgabe:"), new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));
		ausgabeTf = new JTextField(25);
		ausgabeTf.addActionListener(this);
		add(ausgabeTf, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0));

		add(new JLabel(), new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(10, 10, 10, 10), 0, 0));

	}

	public void actionPerformed(ActionEvent e) {
		int zahl;

		try {
			zahl = Integer.parseInt(eingabeTf.getText());
		} catch (NumberFormatException exc) {
			ausgabeTf.setText("Eingabefehler!");
			System.out.println("catch: " + exc.getMessage());
			return;
		} finally {
			System.out.println("finally");
		}

		ausgabeTf.setText("" + 2 * zahl);

//		String tf = "1a, 2.0, 3";
//		double[] v;
//		try {
//			v = stringToCoeff(tf);
//		} catch (NumberFormatException ex) {
//			System.out.println("catch");
//			return;
//		} finally {
//			System.out.println("finally");
//		}
//		System.out.println(Arrays.toString(v));
	}

	private int berechnung(int zahl) throws ValueToBig {
		int resultat = 0;
		// zahl verdoppeln und falls Resultat > 200, Ausnahme werfen

		if ((resultat = 2 * zahl) > 200) {
			throw new ValueToBig("Zahl zu gross", -1);
		}

		return resultat;
	}

//	private double[] stringToCoeff(String s) throws NumberFormatException {
//		String[] tokens = s.split("[, ]+");
//		double[] z = new double[tokens.length];
//		for (int i = 0; i < z.length; i++)
//			z[i] = Double.parseDouble(tokens[i]);
//		return z;
//	}

	public static void main(String args[]) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				ExceptionDemo demo = new ExceptionDemo();
				demo.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				});
				demo.pack();
				demo.setMinimumSize(demo.getPreferredSize());
				demo.setVisible(true);
			}
		});
	}
}

class ValueToBig extends NumberFormatException {
	private static final long serialVersionUID = 1L;
	private int errorID;

	public ValueToBig(String myMessage, int errorID) {
		super(myMessage);
		this.errorID = errorID;
	}

	public int getErrorID() {
		return errorID;
	}
}
