import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class View extends JPanel {
	private Rechteck rechteck = new Rechteck(100, 120, 200, 70);
	private Kreis kreis = new Kreis(10, 20, 70);
	private Form form = (Form) new Kreis(50, 30, 50);

	public View() {
		super(null);
		setPreferredSize(new Dimension(600, 400));

		addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("x: " + e.getX() + " y: " + e.getY());

			}

		});
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		rechteck.zeichne(g);
		kreis.zeichne(g);
	}

}

abstract class Form {
	public int x, y, breite, hoehe;

	public Form(int x, int y, int breite, int hoehe) {
		this.x = x;
		this.y = y;
		this.breite = breite;
		this.hoehe = hoehe;
	}

	public abstract void zeichne(Graphics g);
}

class Rechteck extends Form {

	public Rechteck(int x, int y, int breite, int hoehe) {
		super(x, y, breite, hoehe);
	}

	public void zeichne(Graphics g) {
		g.drawRect(x, y, breite, hoehe);
	}
}

class Kreis extends Form {
	public Kreis(int x, int y, int d) {
		super(x, y, d, d);
	}

	public void zeichne(Graphics g) {
		g.drawOval(x, y, breite, hoehe);
	}
}
