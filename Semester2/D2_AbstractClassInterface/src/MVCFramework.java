import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class MVCFramework extends JFrame {

	public MVCFramework() {
		View view = new View();
		add(view);
		pack();
		setPreferredSize(getMinimumSize());
	}

	public static void main(String args[]) {
		MVCFramework demo = new MVCFramework();
		demo.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		demo.setVisible(true);
		demo.setTitle("MVCFramework");
	}
}
