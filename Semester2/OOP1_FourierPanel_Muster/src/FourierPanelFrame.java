import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import util.Utility;

class FourierPanel extends JPanel implements ActionListener, FocusListener, ItemListener {
	private static final long serialVersionUID = 1L;
	private JTextField tfAmp = new JTextField("1.0");
	private JTextField tfFreq = new JTextField("1.0");
	private JTextField tfHarm = new JTextField("10");
	private JComboBox<String> chForm = new JComboBox<String>();
	private JButton btBerechne = new JButton("Berechne");
	private double[] signal;

	/**
	 * <pre>
	 * - Baut GUI 
	 * - Registriert Listener 
	 * - Ruft actionPerformed(null) aufrufen.
	 * </pre>
	 * </pre>
	 */
	public void init() {
		setLayout(null);
		int row1 = 50;
		int row2 = 175;
		int row3 = 350;
		int row4 = 475;
		int colum1 = 25;
		int colum2 = 75;

		add(new JLabel("Amplitude")).setBounds(row1, colum1, 125, 25);
		add(tfAmp).setBounds(row2, colum1, 125, 25);

		add(new JLabel("Frequenz")).setBounds(row3, colum1, 125, 25);
		add(tfFreq).setBounds(row4, colum1, 125, 25);

		add(new JLabel("Signalform")).setBounds(row1, colum2, 125, 25);
		add(chForm).setBounds(row2, colum2, 125, 25);

		chForm.addItem("Rechteck");
		chForm.addItem("Dreiecke");
		chForm.addItem("S�gezahn");
		chForm.addItem("Trapez");
		chForm.addItem("Puls");

		add(new JLabel("# Harmonische")).setBounds(row3, colum2, 125, 25);
		add(tfHarm).setBounds(row4, colum2, 125, 25);

		add(btBerechne).setBounds(25, 550, 600, 50);
		btBerechne.addActionListener(this);

		tfHarm.addActionListener(this);
		tfHarm.addFocusListener(this);
		tfAmp.addActionListener(this);
		tfAmp.addFocusListener(this);
		tfFreq.addActionListener(this);
		tfFreq.addFocusListener(this);
		chForm.addItemListener(this);

		btBerechne.doClick();
	}

	@Override
	/**
	 * <pre>
	 * - Zeichnet Axen...
	 * - Zeichnet Plot...
	 * - Zeichnet Raster
	 * </pre>
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		int[] x = new int[signal.length];
		int[] y = new int[signal.length];

		for (int i = 0; i < signal.length; i++) {
			x[i] = 25 + i;
			y[i] = (int) (325 - 150 * signal[i]);
		}
		g.setColor(Color.green);
		((Graphics2D) g).setStroke(new BasicStroke(3));
		g.drawPolyline(x, y, x.length);

		g.setColor(Color.black);
		((Graphics2D) g).setStroke(new BasicStroke(1));
		g.drawLine(25, 325, 650, 325); // x Achse
		g.drawLine(25, 125, 25, 525); // Y Achse
	}

	private double[] berechnePulse(double amp, double freq, int nHarm, int N) {
		double[] x = new double[N];
		double alpha = Math.PI / 3.0;
		for (int n = 0; n < N; n++) {
			for (int k = 1; k < nHarm + 2; k++) {
				x[n] += ((4 * amp) / Math.PI) * Math.cos(alpha * (2 * k - 1))
						* Math.sin(2.0 * Math.PI * (2 * k - 1) * freq * n / N) / (2 * k - 1);
			}
		}
		return x;
	}

	private double[] berechneTrapez(double amp, double freq, int nHarm, int N) {
		double[] x = new double[N];
		double alpha = Math.PI / 3.0;
		for (int n = 0; n < N; n++) {
			for (int k = 1; k < nHarm + 2; k++) {
				x[n] += ((4 * amp) / Math.PI) * Math.sin(alpha * (2 * k - 1))
						* Math.sin(2.0 * Math.PI * (2 * k - 1) * freq * n / N) / Math.pow((2 * k - 1), 2.0);
			}
		}
		return x;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Rechtecksignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneRechteck(double amp, double freq, int nHarm, int N) {
		double[] x = new double[N];

		for (int n = 0; n < x.length; n++) {
			for (int k = 1; k < nHarm + 1; k++) {
				x[n] += (4 * amp / Math.PI) * Math.sin(2 * Math.PI * freq * (2 * k - 1) * n / (N - 1))
						/ (2 * k - 1);
			}
		}
		return x;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Dreiecksignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneDreieck(double amp, double freq, int nHarm, int N) {
		double[] x = new double[N];

		for (int n = 0; n < x.length; n++) {
			for (int k = 1; k < nHarm + 1; k++) {
				int m = 0;
				if (k % 2 == 0) {
					m = 1;
				} else {
					m = -1;
				}
				x[n] += m * ((8 * Math.PI) / Math.pow(Math.PI, 2.0) * amp / Math.PI)
						* Math.sin(2 * Math.PI * freq * (2 * k - 1) * n / N) / Math.pow((2 * k - 1), 2.0);
			}
		}

		return x;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Sägezahnsignal
	 * </pre>
	 * 
	 * @param amp
	 * @param freq
	 * @param nHarm
	 * @return
	 */
	private double[] berechneSaegezahn(double amp, double freq, int nHarm, int N) {
		double[] x = new double[N];

		for (int n = 0; n < x.length; n++) {
			for (int k = 1; k < nHarm + 1; k++) {

				x[n] += (2 * amp / Math.PI) * Math.sin(2 * Math.PI * freq * k * n / N) / k
						* Math.pow(-1, k + 1);

			}
		}

		return x;
	}

	/**
	 * <pre>
	 * -Liest Information aus GUI aus.
	 * - Berechnet entsprechendes Signal.
	 * - Legt es in Signal ab.
	 * - löst repaint aus.
	 * </pre>
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		berechne();
		repaint();
	}

	private void berechne() {
		int N = 620; // Anzahl Punkte auf der x Achse
		double freq = Double.parseDouble(tfFreq.getText());
		int nHarm = Integer.parseInt(tfHarm.getText());
		double amp = Double.parseDouble(tfAmp.getText());

		switch (chForm.getSelectedIndex()) {
		case 0:
			signal = berechneRechteck(amp, freq, nHarm, N);
			break;
		case 1:
			signal = berechneDreieck(amp, freq, nHarm, N);
			break;
		case 2:
			signal = berechneSaegezahn(amp, freq, nHarm, N);
			break;
		case 3:
			signal = berechneTrapez(amp, freq, nHarm, N);
			break;
		case 4:
			signal = berechnePulse(amp, freq, nHarm, N);
			break;
		default:
			break;
		}
	}

	@Override
	public void focusGained(FocusEvent e) {
	}

	@Override
	public void focusLost(FocusEvent e) {
		berechne();
		repaint();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		berechne();
		repaint();
	}

}

public class FourierPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static Image icon = Utility.loadResourceImage("apple.png");

	public static void main(String args[]) {
		FourierPanelFrame frame = new FourierPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		FourierPanel view = new FourierPanel();
		view.init();
		frame.add(view);
		frame.setSize(720, 720);
		frame.setIconImage(icon);
		frame.setTitle("|FHNW|EIT|OOP|Fourier-Panel|");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}