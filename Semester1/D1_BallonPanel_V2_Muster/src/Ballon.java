import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Ballon implements MouseListener {
	private TraceV3 tr = new TraceV3(this);
	public static final int ZEICHNEBALLON = 0, ZEICHNEPOP = 1, ZEICHNEPRESENT = 2;
	public int zustand = ZEICHNEBALLON;
	private int x, y, d;
	private Image imBild;
	private Image imPop;
	private Image imPresent;
	private int popcnt = 0;

	public Ballon(int x, int y, int d) {
		tr.constructorCall();
		this.x = x;
		this.y = y;
		this.d = d;
		imBild = Utility.loadResourceImage("Image_" + ((int) (Math.random() * 8 + 1)) + ".png", d, -1);
		imPop = Utility.loadResourceImage("Pop.png", d, -1);
		imPresent = Utility.loadResourceImage("Present.png", d, -1);
	}

	public void anzeigen(Graphics g) {
		tr.paintCall();
		switch (zustand) {
		case ZEICHNEBALLON:
			g.drawImage(imBild, x - d / 2, y - d / 2, null);
			break;
		case ZEICHNEPOP:
			g.drawImage(imPop, x - d / 2, y - d / 2, null);
			break;
		case ZEICHNEPRESENT:
			g.drawImage(imPresent, x - d / 2, y - d / 2, null);
			break;
		}
	}

	public void update() {
		switch (zustand) {
		case ZEICHNEBALLON:
			y--;
			if (y < 75) {
				zustand = ZEICHNEPOP;
				popcnt = 0;
			}
			break;
		case ZEICHNEPOP:
			y++;
			popcnt++;
			if (popcnt == 10) {
				zustand = ZEICHNEPRESENT;
			}
			break;
		case ZEICHNEPRESENT:
			y += 10;
			break;
		}
		if (y > 450) {
			y = 450;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		tr.eventCall();
		if (Math.pow(x - e.getX(), 2.0) + Math.pow(y - e.getY(), 2.0) < Math.pow(20.0, 2.0)) {
			zustand = ZEICHNEPOP;
			popcnt = 0;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}
