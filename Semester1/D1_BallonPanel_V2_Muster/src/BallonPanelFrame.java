import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class BallonPanel extends JPanel implements ActionListener, SimpleTimerListener {
	private TraceV3 tr = new TraceV3(this);
	private JButton btReset = new JButton("Reset");
	private Ballon[] ballon = new Ballon[10];

	public BallonPanel() {
		tr.constructorCall();
		setLayout(null);
		btReset.addActionListener(this);
		add(btReset).setBounds(25, 525, 525, 25);

		for (int i = 0; i < ballon.length; i++) {
			ballon[i] = new Ballon((int) (Math.random() * 500 + 50), (int) (Math.random() * 100 + 350), 100);
			addMouseListener(ballon[i]);
		}
		new SimpleTimer(50, this);
	}

	public void paintComponent(Graphics g) {
		tr.paintCall();
		super.paintComponent(g);

		for (int i = 0; i < ballon.length; i++) {
			ballon[i].anzeigen(g);
		}
	}

	public void actionPerformed(ActionEvent e) {
		tr.eventCall();
		for (int i = 0; i < ballon.length; i++) {
			ballon[i] = new Ballon((int) (Math.random() * 400 + 100), (int) (Math.random() * 100 + 350), 100);
			addMouseListener(ballon[i]);
		}
	}

	@Override
	public void timerAction() {
		for (int i = 0; i < ballon.length; i++) {
			ballon[i].update();
		}
		repaint();
	}
}

public class BallonPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static BallonPanel view = new BallonPanel();

	public static void main(String args[]) {
		BallonPanelFrame frame = new BallonPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.add(view);
		frame.setSize(600, 600);
		frame.setTitle("|FHNW|EIT|OOP|Ballon-Panel|");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}
