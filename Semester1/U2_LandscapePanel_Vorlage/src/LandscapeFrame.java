import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import javax.swing.*;

//extra imports


class LandscapePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//Global Variables
	int pos = -200;

	public void init() {
		new SimpleTimer(50, new SimpleTimerListener() {
			public void timerAction() {
				pos += 2;
				if (pos >= 800)
					pos = -200;
				
				repaint();
			}
		});
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Variables
		int[] cGreen = { 0x7CFC00, 0x7FFF00, 0x32CD32, 0x00FF00, 0x228B22, 0x008000, 0x006400 };

		// background floor
		g.setColor(Color.GREEN);
		g.fillRect(0, 600, 800, 300);

		// background sky
		g.setColor(new Color(0x00ffff)); // light blue
		g.fillRect(0, 0, 800, 600);

		// sun
		g.setColor(Color.YELLOW);
		g.fillOval(100, 100, 100, 100);

		// house
		g.setColor(new Color(0x255));
		g.fillPolygon(new int[] { 100, 175, 250 }, new int[] { 450, 350, 450 }, 3);

		g.setColor(Color.WHITE);
		g.fillRect(100, 450, 150, 200);

		g.setColor(Color.BLACK);
		g.drawRect(125, 480, 25, 25);
		g.drawRect(200, 480, 25, 25);
		g.drawRect(100 + 75, 600, 25, 50);

		// tree
		g.setColor(new Color(0x800000));
		g.fillRect(600, 475, 20, 150);

		// leafs
		for (int i = 0; i < 15; i++) {

			for (int j = 0; j < i; j++) {
				g.setColor(new Color(cGreen[i % 7]));
				g.fillOval((605 - i * 5) + j * 10, 425 + i * 10, 20, 20);
			}
		}
		
		g.setColor(Color.WHITE );
		g.fillOval(pos, 200, 200, 100);

	}

	// Black Magic ;-) ...
	private AffineTransform at = new AffineTransform();
	private boolean sizeSet = false;

	protected void scaleGraphics(Graphics g, double scale) {
		if (!sizeSet) {
			getParent().setSize((int) (scale * 400), (int) (scale * 300));
			sizeSet = true;
		}
		at.setToIdentity();
		at.scale(scale, scale);
		((Graphics2D) g).setTransform(at);
	}
}

public class LandscapeFrame extends JFrame {
	static final long serialVersionUID = 1L;
	LandscapePanel view = new LandscapePanel();

	public LandscapeFrame() {
		setTitle("HelloWorldFrame");
		add(view);
		setSize(800, 800);
		view.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				System.out.println("x: " + e.getX() + " y: " + e.getY());
			}
		});
		view.init();
	}

	public static void main(String args[]) {
		LandscapeFrame frame = new LandscapeFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
