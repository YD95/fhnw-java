public class HelloWorld {

	public static void main(String[] args) {
		// Ausgabe in der Console
		System.out.println("Hello World");
		// Fehlerausgabe in der Console
		System.err.println("Something went wrong ...");
	}
}
