import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class HelloWorldPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public void init() {
		System.out.println("init()");
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		System.out.println("paintComponent()");
		g.setColor(Color.RED);
		g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
		g.setColor(Color.BLACK);
	}
}

public class HelloWorldFrame extends JFrame {
	static final long serialVersionUID = 1L;
	HelloWorldPanel view = new HelloWorldPanel();

	public HelloWorldFrame() {
		setTitle("HelloWorldFrame");
		add(view);
		setSize(400, 300);
		view.init();
	}

	public static void main(String args[]) {
		HelloWorldFrame frame = new HelloWorldFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
