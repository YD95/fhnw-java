import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class ADHocPanel extends JPanel implements ActionListener {
	private TextField tfGr = new TextField("169");
	private TextField tfGw = new TextField("80");
	private TextField tfSex = new TextField("m");
	private Label lbBMI = new Label("24.4");
	private Button btBerechne = new Button("Berechne");

	private Image[] imFrau = new Image[9];
	private Image[] imMann = new Image[9];
	private Image bild;

	/**
	 * <pre>
	 * <b> Baut GUI und l�dt Bilder ... </b>
	 * - Baut GUI
	 * - L�dt Bilder in Arrays:
	 * - F�r i gleich 0 bis i kleiner 9:
	 * 		i-tes Bild mann_i.png mittels Utility - Klasse in i-tes Element von imMann laden
	 * 		i-tes Bild frau_i.png mittels Utility - Klasse in i-tes Element von imFrau laden
	 * - ActionEvent von btBerechne programmatisch ausl�sen.
	 * </pre>
	 */
	public void init() {
		setLayout(null);
		macheGUIZeile("Gr�sse", tfGr, "cm", 0);
		macheGUIZeile("Gewicht", tfGw, "kg", 1);
		macheGUIZeile("Sex", tfSex, "m/w", 2);
		macheGUIZeile("BMI:", lbBMI, "", 3);
		macheGUIZeile("", btBerechne, "", 4);
		btBerechne.addActionListener(this);
		for (int i = 0; i < imFrau.length; i++) {
			imMann[i] = Utility.loadResourceImage("mann_" + i + ".png");
			imFrau[i] = Utility.loadResourceImage("frau_" + i + ".png");
		}
		setSize(400, 450);
	}

	/**
	 * <pre>
	 * <b> Baut eine GUI - Zeile ... </b>
	 * 
	 * - Erzeugt Label mit Text textLabel1 an der Stelle 20, 20 plus zeilenNummer 
	 *   mal 30 mit Breite 80 und H�he 20 hinzu.
	 * - F�gt Komponente component an der Stelle 120, 20 plus zeilenNummer 
	 *   mal 30 mit Breite 180 und H�he 20 hinzu.
	 * - Erzeugt Label mit Text textLabel1 an der Stelle 20, 20 plus zeilenNummer 
	 *   mal 30 mit Breite 80 und H�he 20 hinzu.
	 * </pre>
	 * 
	 * @param txtLb1 Text des ersten Labels
	 * @param comp   Komponente in der Mitte
	 * @param txtLb2 Text des zweiten Labels
	 * @param zeile  Zeilennummer
	 */
	private void macheGUIZeile(String txtLb1, Component comp, String txtLb2, int zeile) {
		add(new Label(txtLb1)).setBounds(20, 20 + zeile * 30, 80, 20);
		add(comp).setBounds(110, 20 + zeile * 30, 180, 20);
		add(new Label(txtLb2)).setBounds(300, 20 + zeile * 30, 80, 20);
	}

	/**
	 * <pre>
	 * <b> Zeichnet das Bild ... </b>
	 * 
	 * 		- bild an der Stelle 110, 170 zeichnen
	 * </pre>
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(bild, 110, 170, this);
	}

	/**
	 * <pre>
	 * <b> Berechnet den BMI auf eine Stelle nach dem Komma ... </b>
	 * 
	 * - BMI berechnen. 
	 * - Wert mal Zehn rechnen, runden und wieder durch Zehn dividieren.
	 * </pre>
	 * 
	 * @param gewicht Gewicht der Person in kg
	 * @param groesse Groesse der Person in cm
	 * @return BMI auf eine Stelle nach dem Komma gerundet.
	 */
	private double berechneBMI(double gewicht, double groesse) {
		return Math.round(10.0 * gewicht / Math.pow(groesse / 100.0, 2.0)) / 10.0;
	}

	/**
	 * <pre>
	 * - Berechnet den zum bmi zugeh�rigen Bild-Index:
	 *   linear zwischen bmi 17 -> index 0 und bmi 33 -> index 8.
	 * - Falls index kleiner Null:
	 *     - index gleich null;
	 * - Falls index gr�sser 8:
	 *     - index gleich 8;
	 * </pre>
	 * 
	 * @param bmi
	 * @return
	 */
	private int berechneBildIndex(double bmi) {
		int index = (int) Math.round(((bmi - 17.0) * 8.0 / 16.0));
		if (index < 0) {
			index = 0;
		}
		if (index > 8) {
			index = 8;
		}
		return index;
	}

	/**
	 * <pre>
	 * <b> Werte aus den GUI - Komponenten auslesen und Berechnungen ausf�hren ... </b>
	 * 
	 *  - Text aus tfGr und tfGw auslesen und in lokale Variabeln ablegen.
	 * 	- Text aus tfSex lesen und in lokalem String sex ablegen.
	 * 	- Mittels berechneBMI() mit den Argumenten gewicht und groesse den BMI berechnen und in lokaler Var. ablegen.
	 * 	- Mittels berechneBildIndex() mit dem Argumenten bmi den Index des zum BMI zugeh�rigen Bildes berechnen in lokaler Var. ablegen.
	 * 	- Falls sex gleich "m"
	 * 		- bild gleich imMann an der Stelle des berechneten Bild-Index setzen.
	 * 	- Sonst 
	 * 		- bild gleich imFrau an der Stelle des berechneten Bild-Index setzen.
	 *  - Neuzeichnen ausl�sen.
	 * 
	 * </pre>
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		double groesse = Double.parseDouble(tfGr.getText());
		double gewicht = Double.parseDouble(tfGw.getText());
		String sex = tfSex.getText();
		double bmi = berechneBMI(gewicht, groesse);
		lbBMI.setText("" + bmi);
		int index = berechneBildIndex(bmi);
		if (sex.equals("m")) {
			bild = imMann[index];
		} else {
			bild = imFrau[index];
		}
		repaint();
	}
}

public class ADHocPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static Image icon = Utility.loadResourceImage("apple.png");

	public static void main(String args[]) {
		ADHocPanelFrame frame = new ADHocPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		ADHocPanel view = new ADHocPanel();
		view.init();
		view.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				System.out.println("x: " + e.getX() + " y: " + e.getY());
			}
		});
		frame.add(view);
		frame.setSize(400, 600);
		frame.setIconImage(icon);
		frame.setTitle("|FHNW|EIT|OOP|Calculation-Panel|");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}