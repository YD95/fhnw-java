import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.sound.sampled.Clip;
import javax.swing.JPanel;

public class Person extends JPanel implements MouseListener {
	private String name = "", vorname = "";
	private Image bild;
	private boolean paintFlag = false;
	private Clip audioClip;

	public Person(String name, String vorname, String bildDatei, String audioDatei) {
		this.name = name;
		this.vorname = vorname;
		bild = Utility.loadResourceImage(bildDatei);
		audioClip = Utility.loadAudioClip(audioDatei);
		addMouseListener(this);
	}

	public void paintComponent(Graphics g) {
		if (paintFlag) {
			g.setColor(Color.pink);
		} else {
			g.setColor(Color.lightGray);
		}
		g.fillRect(5, 5, 205, 340);
		g.setColor(Color.black);
		g.drawString("Name: " + name, 20, 20);
		g.drawString("Vorname: " + vorname, 20, 40);
		g.drawImage(bild, 20, 50, null);
		g.drawRect(5, 5, 205, 340);
	}

	public void mousePressed(MouseEvent e) {
		paintFlag = true;
		audioClip.setFramePosition(0);
		audioClip.start();
		repaint();
	}

	public void mouseReleased(MouseEvent e) {
		paintFlag = false;
		repaint();
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
}
