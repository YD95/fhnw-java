import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

class PersonenPanel extends JPanel {
	private Person person1, person2;

	public PersonenPanel() {
		setLayout(null);
		person1 = new Person("Aniston", "Jennifer", "JenniferAniston.jpg", "catmeow.wav");
		add(person1).setBounds(15, 0, 215, 350);
		person2 = new Person("Leblanc", "Matt", "MattLeblanc.jpg", "lion.wav");
		add(person2).setBounds(215, 0, 215, 350);
	}
}

public class PersonenPanelFrame extends JFrame {
	private PersonenPanel view;

	public PersonenPanelFrame() {
		view = new PersonenPanel();
		add(view);
		setSize(460, 400);
		setTitle("|FHNW|EIT|OOP|Personen - Panel|");
		setResizable(true);
		setVisible(true);
	}

	public static void main(String args[]) {
		PersonenPanelFrame frame = new PersonenPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
}