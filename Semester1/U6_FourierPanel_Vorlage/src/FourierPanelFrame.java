import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class FourierPanel extends JPanel implements ActionListener {

	private static final String def = "1.0";

	private double[] signal;

	private JTextField[] tf = new JTextField[4];
	private JLabel lb0 = new JLabel(def);
	private JLabel lb1 = new JLabel(def);
	private JLabel lb2 = new JLabel(def);
	private JLabel lb3 = new JLabel(def);
	private JLabel lb4 = new JLabel(def);

	private String menu[] = { "rectangle", "triangle", "sawtooth" };

	private JComboBox<String> cbForm = new JComboBox<String>(menu);

	public void init() {
		setLayout(null);

		for (int i = 0; i < tf.length; i++) {
			tf[i] = new JTextField(def);
			tf[i].setFont(new Font("TimesRoman", Font.PLAIN, 24));
		}
		
		signal = calcRect(1.0, 1.0, 2, 580);

		cbForm.setFont(new Font("TimesRoman", Font.PLAIN, 24));

		int y = 960 / 24;
		int x = 1300 / 26;

		lb0.setFont(new Font("TimesRoman", Font.PLAIN, 26));
		lb1.setFont(new Font("TimesRoman", Font.PLAIN, 26));
		lb2.setFont(new Font("TimesRoman", Font.PLAIN, 26));
		lb3.setFont(new Font("TimesRoman", Font.PLAIN, 26));
		lb4.setFont(new Font("TimesRoman", Font.PLAIN, 26));

		add(lb0).setBounds(x, 2 * y, 5 * x, y);
		lb0.setText("Amplitude");
		add(tf[0]).setBounds(5 * x, 2 * y, 5 * x, y);

		add(lb1).setBounds(14 * x, 2 * y, 5 * x, y);
		lb1.setText("Frequency");
		add(tf[1]).setBounds(19 * x, 2 * y, 2 * x, y);
		add(lb2).setBounds(22 * x, 2 * y, 2 * x, y);
		lb2.setText("Hz");

		add(lb3).setBounds(x, 4 * y, 5 * x, y);
		lb3.setText("Form");
		add(cbForm).setBounds(5 * x, 4 * y, 5 * x, y);

		add(lb4).setBounds(14 * x, 4 * y, 5 * x, y);
		lb4.setText("num. over waves");
		add(tf[3]).setBounds(19 * x, 4 * y, 2 * x, y);

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		int[] x = new int[signal.length];
		int[] y = new int[signal.length];

		for (int i = 0; i < y.length; i++) {
			x[i] = 40 + i * 2;
			y[i] = (int) (550 + signal[i] * 160);
		}

		g.drawPolyline(x, y, x.length);

	}

	private double[] calcRect(double amplitude, double frequenz, int harmonische, int N) {
		double[] x = new double[N];

		for (int n = 0; n < x.length; n++) {
			for (int k = 1; k < harmonische + 1; k++) {
				x[n] += (4.0 * amplitude / Math.PI) * Math.sin((2 * k - 1) * 2.0 * Math.PI * frequenz * n / (N - 1))
						/ (2 * k - 1);

			}
		}

		return x; // Platzhalter
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Dreiecksignal
	 * </pre>
	 * 
	 * @param amplitude
	 * @param frequenz
	 * @param harmonische
	 * @return
	 */
	private double[] calcTriangle(double amplitude, double frequenz, int harmonische, int N) {
		double[] x = new double[N];

		return x;
	}

	/**
	 * <pre>
	 * 	-Berechnet aufgrund der Parameter Sägezahnsignal
	 * </pre>
	 * 
	 * @param amplitude
	 * @param frequenz
	 * @param harmonische
	 * @return
	 */
	private double[] calcSaw(double amplitude, double frequenz, int harmonische, int N) {
		double[] x = new double[N];
		return x;
	}

	private void berechne() {
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		signal = calcRect(1.0, 1.0, 2, 580);

	}
}

public class FourierPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static Image icon = Utility.loadResourceImage("fhnw_logo.png");

	public static void main(String args[]) {
		FourierPanelFrame frame = new FourierPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		FourierPanel view = new FourierPanel();
		view.init();
		frame.add(view);
		frame.setSize(1300, 960);
		frame.setIconImage(icon);
		frame.setTitle("Fourier-Panel");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}