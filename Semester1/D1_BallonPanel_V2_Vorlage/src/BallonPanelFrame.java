import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class BallonPanel extends JPanel implements ActionListener, SimpleTimerListener {
	TraceV3 tr = new TraceV3();
	
	private JButton btReset = new JButton("Reset");
	private Ballon[] ballon = new Ballon[10];
	
	public void init() {
		for (int i = 0; i < ballon.length; i++) {
			ballon[i] = new Ballon((int) (Math.random() * 500 + 50), (int) (Math.random() * 100 + 350), 100);
		}
	}

	public BallonPanel() {
		tr.constructorCall();
		
		setLayout(null);
		btReset.addActionListener(this);
		add(btReset).setBounds(25, 525, 525, 25);

		init();
		
		new SimpleTimer(40, this);
	}

	public void paintComponent(Graphics g) {
		tr.paintCall();
		
		super.paintComponent(g);
		for (int i = 0; i < ballon.length; i++) {
			ballon[i].anzeigen(g);
		}
	}

	public void actionPerformed(ActionEvent e) {
//		System.gc();
		tr.eventCall();
		
		init();
		
	}

	@Override
	public void timerAction() {
		for (int i = 0; i < ballon.length; i++) {
			ballon[i].update();
		}
		repaint();
	}
}

public class BallonPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static BallonPanel view = new BallonPanel();

	public static void main(String args[]) {
		BallonPanelFrame frame = new BallonPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.add(view);
		frame.setSize(600, 600);
		frame.setTitle("Ballon-Panel");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}
