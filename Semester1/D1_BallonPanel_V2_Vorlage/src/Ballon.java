import java.awt.Graphics;
import java.awt.Image;

public class Ballon {
	private int x, y, d, s;
	private Image bild;

	public Ballon(int x, int y, int d) {
		this.x = x;
		this.y = y;
		this.d = d;
		this.s = 0;
		bild = Utility.loadResourceImage("Image_" + ((int) (Math.random() * 8 + 1)) + ".png", d, -1);
	}

	public void anzeigen(Graphics g) {
		g.drawImage(bild, x - d / 2, y - d / 2, null);
	}

	public void update() {
		if (y < 25) {
			s++;
		}

		if (s == 0)
			y--;
		else if (s < 8) {
			s++;
			// show pop
		} else {
			// present is falling
			y -= 2;
		}

	}
}
