import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class ADHocPanel extends JPanel implements ActionListener {

	private static final String def = "10.0";

	private JTextField[] tf = new JTextField[2];

//	private String menu[] = {"man","woman","other"};
	private String menu[] = { "man", "woman" };

	private JComboBox<String> mbGen = new JComboBox<String>(menu);

//	private JMenuItem menuMan = new JMenuItem("man");
//	private JMenuItem menuFrau = new JMenuItem("woman");
//	private JMenuItem menuOther = new JMenuItem("other");

	private JButton btCalc = new JButton("Calculate");
	private JLabel lfResult = new JLabel();

	private Image[] iMan = new Image[9];
	private Image[] iFrau = new Image[9];
//	private Image[] iOther = new Image[9];
	private Image picture;

	public void init() {

		for (int i = 0; i < tf.length; i++) {
			tf[i] = new JTextField(def);
		}

		setLayout(null);

		int y = 10;
		add(new JLabel("Size")).setBounds(10, y, 50, 20);
		add(tf[0]).setBounds(60, y, 240, 20);
		add(new JLabel("cm")).setBounds(310, y, 50, 20);
		y += 30;
		add(new JLabel("wight")).setBounds(10, y, 50, 20);
		add(tf[1]).setBounds(60, y, 240, 20);
		add(new JLabel("kg")).setBounds(310, y, 50, 20);
		y += 30;

		add(new JLabel("gender")).setBounds(10, y, 50, 20);
		add(mbGen).setBounds(60, y, 240, 20);
		mbGen.setSelectedIndex(0);
		mbGen.addActionListener(this);

		y += 30;
		add(btCalc).setBounds(60, y, 240, 20);
		btCalc.addActionListener(this);

		y += 30;
		add(new JLabel("BMI:")).setBounds(10, y, 50, 20);
		add(lfResult).setBounds(60, y, 240, 20);

		for (int i = 0; i < iMan.length; i++) {
			iMan[i] = Utility.loadResourceImage("mann_" + i + ".png");
		}

		for (int i = 0; i < iFrau.length; i++) {
			iFrau[i] = Utility.loadResourceImage("frau_" + i + ".png");
		}

//		for (int i = 0; i < iOther.length; i++) {
//			iOther[i] = Utility.loadResourceImage("other_" + i + ".png");
//		}

		picture = Utility.loadResourceImage("fhnw_logo.png", 250, 250);

	}

	public double BMI() {
		double size = Double.parseDouble(this.tf[0].getText()) / 100;
		double wight = Double.parseDouble(this.tf[1].getText());

		return Math.round(wight / (size * size));
	}

	public Image LoadPic(double bmi) {

		Image img;

		double calc = bmi - 18.5;
		double div = (30 - 18.5) / 9;

		int res = (int) (calc / div);

		if (res > 8)
			res = 8;
		else if (res < 0)
			res = 0;

		if (this.mbGen.getSelectedItem() == this.menu[0])
			img = this.iMan[res];
		else
			img = this.iFrau[res];

		return img;
	}

	public boolean check() {

		boolean chk = true;

		for (int i = 0; i < tf.length; i++) {
			try {
				Double.parseDouble(tf[i].getText());
			} catch (NumberFormatException e) {
				chk = false;
			}
		}

		return chk;

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(picture, 40, 200, this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

		if (false == check()) {
			lfResult.setText("Something is wrong!");
			lfResult.setForeground(Color.RED);
			return;
		}

		double bmi = BMI();

		picture = LoadPic(bmi);

		repaint();

		lfResult.setText("" + bmi);
		lfResult.setForeground(Color.BLACK);

	}
}

public class ADHocPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static Image icon = Utility.loadResourceImage("fhnw_logo.png");

	public static void main(String args[]) {
		ADHocPanelFrame frame = new ADHocPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		ADHocPanel view = new ADHocPanel();
		view.init();
		view.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				System.out.println("x: " + e.getX() + " y: " + e.getY());
			}
		});
		frame.add(view);
		frame.setSize(400, 600);
		frame.setIconImage(icon);
		frame.setTitle("BMI Calculation-Panel");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}