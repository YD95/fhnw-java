import java.awt.Graphics;

public class SmartPerson extends Person {
	TraceV3 trace = new TraceV3(this);
	int x = 5; 

	public SmartPerson(String name) {
		super(name);
		trace.constructorCall();
	}

	public void anzeigen(Graphics g) {
		trace.methodeCall();
		g.drawString("Darf ich mich vorstellen: Ich heisse " + name, 10, 200);
		super.anzeigen(g);
	}
}
