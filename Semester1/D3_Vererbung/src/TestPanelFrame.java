import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class TestPanel extends JPanel {
	private TraceV3 tr = new TraceV3(this);
	private SmartPerson smartPerson;

	public TestPanel() {
		tr.constructorCall();
		smartPerson = new SmartPerson("Hans-Heinrich");
	}

	public void paintComponent(Graphics g) {
		tr.methodeCall();
		smartPerson.anzeigen(g);
	}
}

public class TestPanelFrame extends JFrame {
	private TraceV3 tr = new TraceV3(this);
	private static final long serialVersionUID = 1L;
	private TestPanel view;

	public TestPanelFrame() {
		tr.constructorCall();
		view = new TestPanel();
		add(view);
		setSize(600, 600);
		setTitle("|FHNW|EIT|OOP|Ballon-Panel|");
		setResizable(true);
		setVisible(true);
	}

	public static void main(String args[]) {
		TraceV3.mainCall();
		TestPanelFrame frame = new TestPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
}
