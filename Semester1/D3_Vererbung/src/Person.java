import java.awt.Graphics;

public class Person {
	TraceV3 tr = new TraceV3("Person", this);
	protected String name;
	
	public Person(String name) {
		tr.constructorCall();
		this.name = name;
	}

	public void anzeigen(Graphics g) {
		tr.methodeCall();
		g.drawString("Hallo Velo", 10, 170);
	}
}
