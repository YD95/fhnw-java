import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class BasicLayoutPanel extends JPanel {

	private JButton bt1 = new JButton("Button 1");
	private JButton bt2 = new JButton("Button 2");
	private JButton bt3 = new JButton("Button 3");
	private JButton bt4 = new JButton("Button 4");
	private JButton bt5 = new JButton("Button 5");

	public BasicLayoutPanel() {
		super(new GridLayout(3, 2, 20, 20));
		setBorder(MyBorderFactory.createMyBorder("BasicLayoutPanel"));

		add(bt1);
		add(bt2);
		add(new JLabel());
		add(bt3);
		add(bt4);
		add(bt5);
	}

}
