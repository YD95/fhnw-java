import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class BasicControlPanel extends JPanel implements ActionListener, ChangeListener {

	private JLabel lb;
	private JButton bt;
	private JSlider sld;
	private JTextField tf;
	private JComboBox cb;
	private String[] colorList = { "Red", "Green", "Blue" };

	public BasicControlPanel() {
		super(null);
		setBorder(MyBorderFactory.createMyBorder(" Grundlegende Funktionen ... "));
		setPreferredSize(new Dimension(480, 640));

		// JLabel
		lb = new JLabel("<html><b>Gummibärli</b>", Utility.loadResourceIcon("Gummi_gelb.png"), JLabel.CENTER);
		lb.setBackground(Color.CYAN);
		lb.setOpaque(false);
		lb.setVerticalTextPosition(JLabel.BOTTOM);
		lb.setHorizontalTextPosition(JLabel.CENTER);
		add(lb).setBounds(20, 20, 300, 210);

		// JButton
		bt = new JButton("OK", Utility.loadResourceIcon("Gummi_rot.png"));
		bt.setPressedIcon(Utility.loadResourceIcon("Gummi_gruen.png"));
		bt.setDisabledIcon(Utility.loadResourceIcon("Gummi_transparent.png"));
		bt.setVerticalTextPosition(AbstractButton.BOTTOM);
		bt.setHorizontalTextPosition(AbstractButton.CENTER);
		bt.setEnabled(true);
		bt.addActionListener(this);
		add(bt).setBounds(300, 20, 160, 210);

		// JSlider
		sld = new JSlider(JSlider.HORIZONTAL, 0, 30, 15);
		sld.addChangeListener(this);
		sld.setMajorTickSpacing(10);
		sld.setMinorTickSpacing(2);
		sld.setPaintTicks(true);
		sld.setPaintLabels(true);
		add(sld).setBounds(20, 250, 440, 50);

		// JTextField
		tf = new JTextField("JTextField");
		tf.setHorizontalAlignment(JTextField.CENTER);
		tf.addActionListener(this);
		add(tf).setBounds(20, 310, 440, 20);

		// JComboBox
		cb = new JComboBox();
		cb.addActionListener(this);
		for (int i = 0; i < colorList.length; ++i) {
			cb.addItem(colorList[i]);
		}
		add(cb).setBounds(20, 350, 440, 20);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bt) {
			System.out.println("bt");
		}
		if (e.getSource() == tf) {
			System.out.println("tf");
		}
		if (e.getSource() == cb) {
			System.out.println("cb");
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == sld) {
			System.out.println("sld");
		}
	}
}
