import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GridBagLayoutPanelBspV1 extends JPanel {

	private Panel1 panel1 = new Panel1();
	private Panel2 panel2 = new Panel2();
	private Panel3 panel3 = new Panel3();

	public GridBagLayoutPanelBspV1() {
		setLayout(new GridBagLayout());
		setBorder(MyBorderFactory.createMyBorder("Beispiel 1"));

		add(panel1, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(5, 5, 5, 5), 5, 5));

		add(panel2, new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
				new Insets(5, 5, 5, 5), 5, 5));

		add(panel3, new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 5, 5));

	}
}

class Panel1 extends JPanel {

	public Panel1() {
		setBorder(MyBorderFactory.createMyBorder("Panel 1"));
	}
}

class Panel2 extends JPanel {
	private JTextField tf1 = new JTextField("Hallo Velo");
	private JTextField tf2 = new JTextField("Hallo E-Bike");

	public Panel2() {
		super(new GridBagLayout());
		setBorder(MyBorderFactory.createMyBorder("Panel 2"));

		add(new JLabel("Label 1:"), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.LINE_START,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 5, 5));
		
		add(tf1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.LINE_START,
				GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 5, 5));
		
		add(new JLabel(""), new GridBagConstraints(0, 1, 2, 1, 0.0, 1.0, GridBagConstraints.LINE_START,
				GridBagConstraints.VERTICAL, new Insets(5, 5, 5, 5), 5, 5));
	}
}

class Panel3 extends JPanel {

	public Panel3() {
		setBorder(MyBorderFactory.createMyBorder("Panel 3"));
	}
}