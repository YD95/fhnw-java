import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextField;

public class StatusBar extends JPanel {

	public static JTextField tf = new JTextField();

	public StatusBar() {
		super(new BorderLayout());
		setBorder(MyBorderFactory.createMyBorder("StatusBar"));
		add(tf, BorderLayout.CENTER);
	}
}
