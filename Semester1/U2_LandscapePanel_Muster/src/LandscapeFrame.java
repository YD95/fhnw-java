import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import javax.swing.*;

class LandscapePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private double scale = 1.0;
	private int updateCnt = 0;

	private Image imWolke = Utility.loadResourceImage("rockabilly-girl.jpg", 100, -1);

	public void init() {
		// Hintergrundsfarbe blau setzen ...
		setBackground(new Color(145, 245, 245));
		new SimpleTimer(50, new SimpleTimerListener() {
			public void timerAction() {
				repaint();
			}
		});
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		scaleGraphics(g, scale);

		updateCnt++;
		System.out.println("paintComponent()");

		// updateCnt inkrementieren und den Wert von updateCnt lokaler Var
		// xWolke zuordnen.

		int xWolke = updateCnt;

		// Landschaft zeichnen
		g.setColor(Color.green);
		g.fillRect(0, 175, 400, 125);
		g.setColor(Color.yellow);
		g.fillArc(50, 25, 25, 25, 0, 360);

		g.setColor(Color.black);
		g.drawRect(50, 120, 50, 55);
		g.setColor(Color.white);
		g.fillRect(50, 120, 50, 55);
		g.setColor(Color.black);
		g.drawRect(80, 150, 10, 25);
		g.drawRect(60, 130, 10, 10);
		g.drawLine(75, 95, 40, 130);
		g.drawLine(75, 95, 110, 130);
		g.setColor(Color.darkGray);
		int[] xpoints = { 75, 50, 100 };
		int[] ypoints = { 95, 120, 120 };
		g.fillPolygon(xpoints, ypoints, 3);

		Color braun = new Color(100, 100, 0);
		g.setColor(braun);
		g.fillRect(200, 125, 8, 60);

		g.setColor(Color.green);
		g.fillOval(175, 90, 20, 30);
		g.fillOval(185, 80, 35, 50);
		g.fillOval(210, 85, 25, 30);
		g.fillOval(200, 105, 25, 30);
		g.fillOval(180, 105, 25, 30);

		// Wolke in Funktion von xWolke zeichnen
//		g.setColor(Color.white);
//		g.fillOval(xWolke + 0, 40, 50, 25);
//		g.fillOval(xWolke + 25, 40, 50, 25);
//		g.fillOval(xWolke + 50, 25, 25, 15);
//		g.fillOval(xWolke + 75, 20, 75, 10);

		g.drawImage(imWolke, xWolke, 20, this);
	}

	// Black Magic ;-) ...
	private AffineTransform at = new AffineTransform();
	private boolean sizeSet = false;

	private void scaleGraphics(Graphics g, double scale) {
		if (!sizeSet) {
			getParent().setSize((int) (scale * 400), (int) (scale * 300));
			sizeSet = true;
		}
		at.setToIdentity();
		at.scale(scale, scale);
		((Graphics2D) g).setTransform(at);
	}
}

public class LandscapeFrame extends JFrame {
	static final long serialVersionUID = 1L;
	LandscapePanel view = new LandscapePanel();

	public LandscapeFrame() {
		setTitle("HelloWorldFrame");
		add(view);
		setSize(400, 300);
		view.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				System.out.println("x: " + e.getX() + " y: " + e.getY());
			}
		});
		view.init();
	}

	public static void main(String args[]) {
		LandscapeFrame frame = new LandscapeFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.setResizable(true);
		frame.setVisible(true);
	}
}
