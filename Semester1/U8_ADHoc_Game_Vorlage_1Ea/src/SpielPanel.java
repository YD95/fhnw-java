import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class SpielPanel extends JPanel implements KeyListener, SimpleTimerListener {
	private boolean stop = false;
	private Schlaeger schlaeger;
	private Ball ball;
	private SimpleTimer timer;

	/**
	 * <pre>
	 * - Erzeugt den Schlaeger an geeigneter x,y - Position und mit dem entsprechenden Bild.
	 * - Erzeugt den Ball mit zufälliger x,y - Position und mit dem entsprechenden Bild.
	 * - Setzt mittels setRechteck() die Umrandung des Balls.
	 * - Erzeugt den SimpleTimer timer.
	 * </pre>
	 */
	public SpielPanel() {
		addKeyListener(this);
		ball = new Ball(zufall(50, 500), zufall(50, 680), -5, -5, Utility.loadResourceImage("ball.png", 60, -1));
		schlaeger = new Schlaeger(900, 375, Utility.loadResourceImage("schlaeger.png"));
		ball.setRechteck(new Rectangle(20, 20, 960, 710));
		timer = new SimpleTimer(50, this);
	}

	/**
	 * - Zeichnet Schlaeger und ball mittels anzeigen() zeichnen.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		ball.anzeigen(g);
		schlaeger.anzeigen(g);
	}

	/**
	 * - Setzt entsprechndes Attribut
	 * 
	 * @param stop
	 */
	public void setStop(boolean stop) {
		this.stop = stop;
	}

	/**
	 * -Erzeugt eine Zufallszahl im Bereich von ... bis inklusive der Grenzen.
	 * 
	 * @param von
	 * @param bis
	 * @return
	 */
	protected int zufall(int von, int bis) {

		return (int) ((bis - von + 1) * Math.random() + von); // Ersetzen !!!
	}

	/**
	 * - Ruft update() des Balls auf. - Ruft kollisionTesten() des Schlaegers auf.
	 */
	@Override
	public void timerAction() {
		ball.update();
		schlaeger.kollisionTesten(ball);
		repaint();
	}

	/**
	 * Je nach Pfeiltaste Schlaeger nach oben oder unten verschieben.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			System.out.println("up");
			schlaeger.setPosition(900, schlaeger.y - 10);
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			System.out.println("up");
			schlaeger.setPosition(900, schlaeger.y + 10);
		}
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
}
