import java.awt.Image;

public class BewegtesObjekt extends BildObjekt {
	protected int vx, vy;

	/**
	 * <pre>
	 * - Ruft geeigneten Konstruktor der Superklasse auf.
	 * - Setzt verbleibende Attribute.
	 * </pre>
	 * 
	 * @param x
	 * @param y
	 * @param vx
	 * @param vy
	 * @param bild
	 */
	public BewegtesObjekt(int x, int y, int vx, int vy, Image bild) {
		super(x, y, bild);
		this.vx = vx;
		this.vy = vy;
	}

	/**
	 * - Verschiebt Objekt entsprechend vx und vy.
	 */
	public void update() {
		x += vx;
		y += vy;
	}
}
