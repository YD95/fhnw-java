import java.awt.Image;

public class Schlaeger extends BildObjekt {

	/**
	 * - Ruft entsprechenden Konstruktor der Superklasse auf.
	 * 
	 * @param x
	 * @param y
	 * @param bild
	 */
	public Schlaeger(int x, int y, Image bild) {
		super(x, y, bild);
	}

	/**
	 * Detektiert Kollision von Ball mit Schlaeger und gibt im Falle einer Kollision
	 * wahr zur�ck.
	 * 
	 * @param obj
	 * @return
	 */
	public boolean kollisionTesten(BewegtesObjekt ball) {
		if (ball.y > y - hoehe / 2 && ball.y < y + hoehe / 2 && x - ball.x < breite / 2 + ball.breite / 2) {
			ball.vx *= -1;
			return true;
		}
		return false; // Ersetzen!!!
	}

	/**
	 * - Setzt entsprechende Attribute.
	 * 
	 * @param x
	 * @param y
	 */
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

}
