import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class TopViewPanel extends JPanel implements ActionListener {
	private SpielPanel spielPanel = new SpielPanel();
	private JButton btStop = new JButton("Stop");

	/**
	 * <pre>
	 * - F�gt SpielPanel an geeigneter Stelle hinzu.
	 * - F�gt den JButton an geeigneter Stelle ein.
	 * - Registriert das SpielPanel als KeyListener beim JButton.
	 * </pre>
	 */
	public TopViewPanel() {
		setLayout(null);
		add(spielPanel).setBounds(0, 0, 1000, 750);
		btStop.addKeyListener(spielPanel);
		add(btStop).setBounds(20, 775, 750, 25);
	}

	/**
	 * - Ruft entsprechende Methode des SpielPanel auf. Button soll toggeln.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

	}

}
