import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

public class Ball extends BewegtesObjekt {
	protected Rectangle rect;

	/**
	 * - Ruft den passenden Konstruktor der Superklasse auf.
	 * 
	 * @param x
	 * @param y
	 * @param vx
	 * @param vy
	 * @param bild
	 */
	public Ball(int x, int y, int vx, int vy, Image bild) {
		super(x, y, vx, vy, bild);
	}

	public void anzeigen(Graphics g) {
		g.drawRect(rect.x, rect.y, rect.width, rect.height);
		super.anzeigen(g);
	}

	public void update() {
		if (x - rect.x < breite / 2) {
			vx *= -1;
		}
		if (y - rect.y < hoehe / 2) {
			vy *= -1;
		}
		if (rect.y + rect.height - y < hoehe / 2) {
			vy *= -1;
		}
		super.update();
	}

	public void setRechteck(Rectangle rect) {
		this.rect = rect;
	}
}
