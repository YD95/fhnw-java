import java.awt.Graphics;

import javax.swing.JPanel;

public class UhrPanel extends JPanel implements SimpleTimerListener {
	private static final long serialVersionUID = 1L;
	
	private SimpleTimer timer;
	private Clock clock;

	private Segment seg = new Segment(10, 10, Segment.HOR);
	
	public UhrPanel() {
	}

	@Override
	public void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		seg.setState(true);
		seg.show(g);

	}

	@Override
	public void timerAction() {
		// TODO Auto-generated method stub

	}

}
