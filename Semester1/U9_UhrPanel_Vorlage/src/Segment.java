import java.awt.Color;
import java.awt.Graphics;

public class Segment extends GraficObject {

	static int HOR = 0;
	static int VER = 1;

	protected int orientation;
	protected boolean on = false;

	private int xPoints[] = { 0, 0, 4, 12, 32, 36, 36, 32 };
	private int yPoints[] = { 4, 8, 12, 12, 8, 4, 0, 0 };

	public Segment(int x, int y, int orientation) {
		super(x, y);

		this.orientation = orientation;

		if (orientation == HOR) {
			super.x = x + 2;
			super.y = y - 6;
			int xPoints[] = { 0 + x, 0 + x, 4 + x, 12 + x, 32 + x, 36 + x, 36 + x, 32 + x };
			int yPoints[] = { 4 + y, 8 + y, 12 + y, 12 + y, 8 + y, 4 + y, 0 + y, 0 + y };
			this.xPoints = xPoints;
			this.yPoints = yPoints;
			// offx,offy,9*4,3*4

		} else {
			super.y = y + 2;
			super.x = x - 6;

			// offx, offy, 3*4,9*4
		}

	}

	public void show(Graphics g) {

		g.setColor(Color.RED);

		if (this.orientation == HOR) {
			g.fillPolygon(this.xPoints, this.yPoints, 8);
		} else {
			g.fillRect(this.x, this.y, 3 * 4, 9 * 4);
		}

	}

	public void setState(boolean on) {

	}

}
