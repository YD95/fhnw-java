import java.awt.Graphics;

public class SevenSegment extends GraficObject{

	protected int x, y;
	
	private Segment[] segment = new Segment[7];

	public SevenSegment(int x, int y) {
		super(x,y);
		
		segment[0] = new Segment(10, 10, Segment.HOR);

	}

	public void show(Graphics g) {
		
		segment[0].show(g);

	}

	public void setVal(int num) {

	}
}
