import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class GrundBausteinePanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public void init() {
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}
}

public class GrundbausteineFrame extends JFrame {
	static final long serialVersionUID = 1L;
	GrundBausteinePanel view = new GrundBausteinePanel();

	public GrundbausteineFrame() {
		setTitle("GrundbausteineFrame");
		add(view);
		setSize(400, 300);
		view.init();
	}

	public static void main(String args[]) {
		GrundbausteineFrame frame = new GrundbausteineFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
