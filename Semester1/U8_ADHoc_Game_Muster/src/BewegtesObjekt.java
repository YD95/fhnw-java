import java.awt.Image;

public class BewegtesObjekt extends BildObjekt {
	protected double vx, vy;

	public BewegtesObjekt(int x, int y, Image bild) {
		super(x, y, bild);
	}

	public BewegtesObjekt(int x, int y, double vx, double vy, Image bild) {
		super(x, y, bild);
		this.vx = vx;
		this.vy = vy;
	}

	public void update() {
		x += vx;
		y += vy;
	}
}
