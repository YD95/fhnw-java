import java.awt.Graphics;
import java.awt.Image;

public class BildObjekt {
	protected int x, y, breite, hoehe;
	protected Image bild;

	public BildObjekt(int x, int y, Image bild) {
		this.x = x;
		this.y = y;
		this.bild = bild;
		breite = bild.getWidth(null);
		hoehe = bild.getHeight(null);
	}

	public void anzeigen(Graphics g) {
		g.drawImage(bild, x - breite / 2, y - hoehe / 2, null);
	}
}
