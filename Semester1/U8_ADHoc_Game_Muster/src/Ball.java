import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

public class Ball extends BewegtesObjekt {
	protected static Rectangle rect;

	public Ball(int x, int y, int vx, int vy, Image bild) {
		super(x, y, vx, vy, bild);
	}

	public void anzeigen(Graphics g) {
		g.drawRect(rect.x, rect.y, rect.width, rect.height);
		super.anzeigen(g);
	}

	public void setRechteck(Rectangle rect) {
		Ball.rect = rect;
	}

	public void update() {
		super.update();
		if (x < rect.x + breite / 2) {
			vx *= -1;
			x = rect.x + breite / 2;
		}
		if (y < rect.y + hoehe / 2) {
			vy *= -1;
			y = rect.y + hoehe / 2;
		}
		if (y > rect.y + rect.height - hoehe / 2) {
			vy *= -1;
			y = rect.y + rect.height - hoehe / 2;
		}
	}
}
