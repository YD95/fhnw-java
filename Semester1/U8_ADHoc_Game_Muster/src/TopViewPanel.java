import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class TopViewPanel extends JPanel implements ActionListener {
	private JButton btStartStop = new JButton("Stop");
	private SpielPanel spielPanel;

	public TopViewPanel() {
		setLayout(null);
		spielPanel = new SpielPanel();
		add(spielPanel).setBounds(0, 0, 1000, 700);

		add(btStartStop).setBounds(110, 730, 780, 20);
		btStartStop.addActionListener(this);
		btStartStop.addKeyListener(spielPanel);
	}

	public void actionPerformed(ActionEvent e) {
		if (btStartStop.getText().equals("Start")) {
			btStartStop.setText("Stop");
			spielPanel.setStop(false);
		} else {
			btStartStop.setText("Start");
			spielPanel.setStop(true);
		}
	}
}
