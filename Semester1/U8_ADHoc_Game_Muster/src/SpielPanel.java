
// Name:
// Vorname:

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class SpielPanel extends JPanel implements SimpleTimerListener, KeyListener {
	private SimpleTimer timer;
	private Schlaeger schlaeger = new Schlaeger(900, 400, Utility.loadResourceImage("schlaeger.png", 25, 100));
	private Ball ball;
	private boolean stop;

	public SpielPanel() {
		super();
		ball = new Ball(150, 350, zufall(-10, -5), zufall(-5, -1), Utility.loadResourceImage("ball.png", 50, 50));
		ball.setRechteck(new Rectangle(5, 5, 980, 690));
		timer = new SimpleTimer(50, this);
		addKeyListener(this);
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) {
			schlaeger.y -= 20;
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			schlaeger.y += 20;
		}
		if (schlaeger.y > 645) {
			schlaeger.y = 645;
		}
		if (schlaeger.y < 55) {
			schlaeger.y = 55;
		}
		repaint();
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		ball.anzeigen(g);
		schlaeger.anzeigen(g);
	}

	public void timerAction() {
		if (!stop) {
			ball.update();
			schlaeger.kollisionTesten(ball);
			if (ball.x > 1200) {
				ball = new Ball(150, 350, zufall(-10, -5), zufall(-5, -1),
						Utility.loadResourceImage("ball.png", 50, 50));
			}
			repaint();
		}
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	protected int zufall(int von, int bis) {
		return (int) (Math.random() * (bis - von + 1)) + von;
	}

}
