import java.awt.Image;

public class Schlaeger extends BildObjekt {

	public Schlaeger(int x, int y, Image bild) {
		super(x, y, bild);
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public boolean kollisionTesten(BewegtesObjekt bewegtesObjekt) {
		if (Math.abs(x - bewegtesObjekt.x) < (breite + bewegtesObjekt.breite) / 2
				&& Math.abs(y - bewegtesObjekt.y) < (hoehe + bewegtesObjekt.hoehe) / 2) {
			bewegtesObjekt.vx *= -1;
			bewegtesObjekt.x = x - (breite + bewegtesObjekt.breite) / 2;
			return true;
		} else
			return false;
	}
}
