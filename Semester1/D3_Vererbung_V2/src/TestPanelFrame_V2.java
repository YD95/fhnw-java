import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class TestPanel extends JPanel {
	private TraceV3 tr = new TraceV3(this);
	private SmartPerson smartPerson;

	public TestPanel() {
		super();
		tr.constructorCall();
		smartPerson = new SmartPerson("Hans-Heinrich");
	}

	public void paintComponent(Graphics g) {
		tr.paintCall();
		smartPerson.anzeigen(g);
	}
}

class SmartPerson extends Person {
	private TraceV3 tr = new TraceV3(this);
	int x = 5;
	int y = 0;

	public SmartPerson(String name) {
		super(name);
		tr.constructorCall();
		x = 7;
	}

	public void anzeigen(Graphics g) {
		tr.paintCall();
		g.drawString("Darf ich mich vorstellen: Ich heisse " + name, 10, 200);
		super.anzeigen(g);
	}
}

class Person extends Object {
	private TraceV3 tr = new TraceV3("Person", this);
	protected String name;
	protected int matrikelNummer = 16;

	public Person(String name) {
		super();
		tr.constructorCall();
		this.name = name;
	}

	public void anzeigen(Graphics g) {
		tr.paintCall();
		g.drawString("Hallo Velo", 10, 170);
	}
}

public class TestPanelFrame_V2 extends JFrame {
	private TraceV3 tr = new TraceV3(this);
	private static final long serialVersionUID = 1L;
	private TestPanel view = null;

	public TestPanelFrame_V2() {
		super();
		tr.constructorCall();
		view = new TestPanel();
		add(view);
		setSize(600, 600);
		setTitle("|FHNW|EIT|OOP|Ballon-Panel|");
		setResizable(true);
		setVisible(true);
	}

	public static void main(String args[]) {
		TraceV3.mainCall();
		TestPanelFrame_V2 frame = new TestPanelFrame_V2();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
}
