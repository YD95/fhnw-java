import java.awt.Color;
import java.awt.Graphics;

public class Segment extends GrafikObjekt {
	protected int orientierung;
	public static final int HORIZONTAL = 0, VERTIKAL = 1;
	protected boolean ein = false;

	public Segment(int x, int y, int orientierung) {
		super(x, y);
		this.orientierung = orientierung;
	}

	public void anzeigen(Graphics g) {
		if (ein) {
			g.setColor(new Color(0xff, 0x0, 0x0));
		} else {
			g.setColor(new Color(0x38, 0x38, 0x38));
		}
		if (orientierung == HORIZONTAL) {
			int[] px = { 2 + x, 6 + x, 34 + x, 38 + x, 38 + x, 34 + x, 6 + x, 2 + x };
			int[] py = { -2 + y, -6 + y, -6 + y, -2 + y, 2 + y, 6 + y, 6 + y, 2 + y };
			g.fillPolygon(px, py, px.length);
		}
		if (orientierung == VERTIKAL) {
			int[] px = { -2 + x, -6 + x, -6 + x, -2 + x, 2 + x, 6 + x, 6 + x, 2 + x };
			int[] py = { 2 + y, 6 + y, 34 + y, 38 + y, 38 + y, 34 + y, 6 + y, 2 + y };
			g.fillPolygon(px, py, px.length);
		}
	}

	public void setzeZustand(boolean ein) {
		this.ein = ein;
	}
}
