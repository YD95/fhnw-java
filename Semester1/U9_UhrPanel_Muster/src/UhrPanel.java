import java.awt.Graphics;

import javax.swing.JPanel;

public class UhrPanel extends JPanel implements SimpleTimerListener {
	private static final long serialVersionUID = 1L;
	private SimpleTimer timer;
	private Uhr uhr;

	public UhrPanel() {
		uhr = new Uhr(10, 10);
		timer = new SimpleTimer(1000, this);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		uhr.anzeigen(g);
	}

	@Override
	public void timerAction() {
		uhr.setzeZeit();
		repaint();
	}
}
