import java.awt.Graphics;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Uhr extends GrafikObjekt {
	private int stunde, minute, sekunde;
	private SiebenSegment[] siebenSegment = new SiebenSegment[6];

	public Uhr(int x, int y) {
		super(x, y);

		int px = 8;
		siebenSegment[5] = new SiebenSegment(px, 8);
		px += 60;
		siebenSegment[4] = new SiebenSegment(px, 8);
		px += 64;
		siebenSegment[3] = new SiebenSegment(px, 8);
		px += 60;
		siebenSegment[2] = new SiebenSegment(px, 8);
		px += 64;
		siebenSegment[1] = new SiebenSegment(px, 8);
		px += 60;
		siebenSegment[0] = new SiebenSegment(px, 8);
		px += 60;
	}

	public void anzeigen(Graphics g) {
		for (int i = 0; i < siebenSegment.length; i++) {
			siebenSegment[i].anzeigen(g);
		}
	}

	public void setzeZeit() {
		GregorianCalendar calendar = new GregorianCalendar();
		stunde = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
		sekunde = calendar.get(Calendar.SECOND);

		siebenSegment[0].setzeWert(sekunde % 10);
		siebenSegment[1].setzeWert(sekunde / 10);
		siebenSegment[2].setzeWert(minute % 10);
		siebenSegment[3].setzeWert(minute / 10);
		siebenSegment[4].setzeWert(stunde % 10);
		siebenSegment[5].setzeWert(stunde / 10);
	}
}
