import java.awt.Frame;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ApplicationFramework extends Frame {
	private static final long serialVersionUID = 1L;
	private UhrApplet view = new UhrApplet();
	private Image icon = Utility.loadResourceImage("Kopf.png"); 

	public ApplicationFramework() {
		view.init();
		add(view);
		setIconImage(icon);
		setTitle("|FHNW|EIT|OOP|Analog Uhr|");
		setSize(400, 300);
		setResizable(false);
		validate();
		setVisible(true);
	}

	public static void main(String[] args) {
		Frame frame = new ApplicationFramework();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
}
