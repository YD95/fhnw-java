// Ich bestaetige, dass ich diese Pruefung selbstaendig geloest habe.
// Ich weiss, dass bei Zuwiederhandlung die Note 1 erteilt wird.
//
// Name:
// Vorname:
// Klasse:
//

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

public class UhrApplet extends Applet {
    private BufferedImage bufferedImage;

    public UhrApplet() {
        // Hier folgt ihr Code:

    }

    /**
     * Double-Buffering erlaubt flickerfreies Zeichnen. Diese Methode muss nicht geaendert werden.
     *
     * @param g Graphics
     */
    public void update(Graphics g) {
        if (bufferedImage == null)
            bufferedImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        bufferedImage.getGraphics().setColor(this.getBackground());
        bufferedImage.getGraphics().fillRect(0, 0, this.getWidth(), this.getHeight());
        bufferedImage.getGraphics().setColor(g.getColor());
        paint((bufferedImage.getGraphics()));
        g.drawImage(bufferedImage, 0, 0, null);
    }

}
