import java.awt.*;

import javax.swing.*;

public class Utility {

    private static Container p = new Container();

    public static Image loadImage(String strBild) {
        MediaTracker tracker = new MediaTracker(p);
        Image img = (new ImageIcon(strBild)).getImage();
        tracker.addImage(img, 0);
        try {
            tracker.waitForID(0);
        } catch (InterruptedException ex) {
            System.out.println("Can not load image: " + strBild);
        }
        return img;
    }

    public static Image loadResourceImage(String strBild) {
        MediaTracker tracker = new MediaTracker(p);
        Image img = (new ImageIcon(Utility.class.getResource("bilder/" + strBild))).getImage();
        tracker.addImage(img, 0);
        try {
            tracker.waitForID(0);
        } catch (InterruptedException ex) {
            System.out.println("Can not load image: " + strBild);
        }
        return img;
    }

}
