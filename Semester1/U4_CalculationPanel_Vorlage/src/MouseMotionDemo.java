import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

class MouseMotionDemoPanel extends JPanel implements MouseMotionListener {
	private static final long serialVersionUID = 1L;

	public void init() {

		addMouseMotionListener(this);

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

		System.out.println("draging x = " + e.getX() + " and y = " + e.getY());

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

		System.out.println("x: " + e.getX() + " y: " + e.getY());

	}
}

public class MouseMotionDemo extends JFrame {
	private static final long serialVersionUID = 1L;

	public static void main(String args[]) {
		MouseMotionDemo frame = new MouseMotionDemo();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		MouseMotionDemoPanel view = new MouseMotionDemoPanel();
		frame.add(view);
		frame.setSize(800, 800);
		view.init();
		frame.setResizable(false);
		frame.setVisible(true);
	}
}