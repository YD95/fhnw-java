import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class CalculationPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private static final int HORIZONTAL = 0, VERTIKAL = 1;

	private static final String def = "10.0";

	private JTextField[] tfr = new JTextField[4];

//	private JTextField tfR1 = new JTextField(def);
//	private JTextField tfR2 = new JTextField(def);
//	private JTextField tfR3 = new JTextField(def);
//	private JTextField tfR4 = new JTextField(def);
	private JTextField tfV1 = new JTextField(def);

	private JButton btCalc = new JButton("Calculate");

	private JLabel lfresult = new JLabel();

	public void init() {

		for (int i = 0; i < tfr.length; i++) {

			tfr[i] = new JTextField(def);

		}

		setLayout(null);

		btCalc.addActionListener(this);

		int y = 10;
		for (int i = 0; i < tfr.length; i++) {
			add(new JLabel("R" + (i + 1) + ": ")).setBounds(10, y, 50, 20);
			add(tfr[i]).setBounds(60, y, 240, 20);
			add(new JLabel("Ohm")).setBounds(310, y, 50, 20);
			y += 30;
		}

//		add(new JLabel("R1: ")).setBounds(10, y, 50, 20);
//		add(tfR1).setBounds(60, y, 240, 20);
//		add(new JLabel("Ohm")).setBounds(310, y, 50, 20);
//
//		y += 30;
//		add(new JLabel("R2: ")).setBounds(10, y, 50, 20);
//		add(tfR2).setBounds(60, y, 240, 20);
//		add(new JLabel("Ohm")).setBounds(310, y, 50, 20);
//
//		y += 30;
//		add(new JLabel("R3: ")).setBounds(10, y, 50, 20);
//		add(tfR3).setBounds(60, y, 240, 20);
//		add(new JLabel("Ohm")).setBounds(310, y, 50, 20);
//
//		y += 30;
//		add(new JLabel("R4: ")).setBounds(10, y, 50, 20);
//		add(tfR4).setBounds(60, y, 240, 20);
//		add(new JLabel("Ohm")).setBounds(310, y, 50, 20);

		add(new JLabel("U: ")).setBounds(10, y, 50, 20);
		add(tfV1).setBounds(60, y, 240, 20);
		add(new JLabel("Volts")).setBounds(310, y, 50, 20);

		y += 30;
		add(btCalc).setBounds(60, y, 240, 20);

		y += 30;
		add(lfresult).setBounds(60, y, 240, 20);

	}

	public boolean check() {

		boolean chk = true;

		for (int i = 0; i < tfr.length; i++) {
//			if (tfr[i].getText().equals(""))
//				chk = false;

			try {
				Double.parseDouble(tfr[i].getText());
			} catch (NumberFormatException e) {
				chk = false;
			}
		}

//		if (tfV1.getText().equals(""))
//			chk = false;

		try {
			Double.parseDouble(tfV1.getText());
		} catch (NumberFormatException e) {
			chk = false;
		}

		return chk;

	}

	public void resistor(Graphics g, int x, int y, String txt) {

		g.setColor(Color.BLACK);
		g.drawLine(x, y, x, y + 20);
		g.drawRect(x - 10, y + 20, 20, 40);
		g.drawLine(x, y + 60, x, y + 80);
		g.drawString(txt, x + 15, y + 45);

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		resistor(g, 180, 220, "R1");
		resistor(g, 180, 300, "R2");
		resistor(g, 140, 380, "R3");
		resistor(g, 220, 380, "R4");
		g.drawLine(140, 380, 220, 380);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (false == check()) {
			lfresult.setText("Something is wrong!");
			lfresult.setForeground(Color.RED);
			return;
		}

		double r[] = new double[tfr.length];

		for (int i = 0; i < tfr.length; i++) {

			r[i] = Double.parseDouble(tfr[i].getText());
		}

//		double r1 = Double.parseDouble(tfR1.getText());
//		double r2 = Double.parseDouble(tfR2.getText());
//		double r3 = Double.parseDouble(tfR3.getText());
//		double r4 = Double.parseDouble(tfR4.getText());
		double v1 = Double.parseDouble(tfV1.getText());

		double vr1 = (v1 / (r[0] + r[1] + (1 / ((1 / r[2]) + (1 / r[3]))))) * r[0];

		lfresult.setText("Voltage at R1: " + vr1 + "VDC");
		lfresult.setForeground(Color.BLACK);

	}
}

public class CalculationPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static Image icon = Utility.loadResourceImage("fhnw_logo.png");

	public static void main(String args[]) {
		MouseMotionDemo frame = new MouseMotionDemo();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		CalculationPanel view = new CalculationPanel();
		view.init();
		view.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				System.out.println("x: " + e.getX() + " y: " + e.getY());
			}
		});
		frame.add(view);
		frame.setSize(400, 600);
		frame.setIconImage(icon);
		frame.setTitle("FHNW EIT Calculation-Panel");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}