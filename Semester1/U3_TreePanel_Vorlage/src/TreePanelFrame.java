import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.beans.PropertyChangeListener;

import javax.swing.*;
import javax.swing.text.View;

import org.omg.PortableInterceptor.ACTIVE;

import com.sun.java.swing.plaf.windows.WindowsBorders;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import sun.swing.plaf.WindowsKeybindings;

class TreePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private int a = 100;
	private int[] xPos = new int[a];
	private int[] yPos = new int[a];
	private int[] defColor = new int[a];

	public void init() {

		int[] cGreen = { 0x7CFC00, 0x7FFF00, 0x32CD32, 0x00FF00, 0x228B22, 0x008000, 0x006400 };

		for (int i = 0; i < a; i++) {
			xPos[i] = (int) (Math.random() * 3600) % 1200;
			yPos[i] = (int) ((Math.random() * 3600) % 700) + 100;
			defColor[i] = cGreen[(int) ((Math.random() * 777.0) % 7)];
		}

		for (int i = 0, j = 0, cl = 0; i < yPos.length - 1; i++) {
			if (yPos[i] > yPos[i + 1]) {
				j = yPos[i];
				yPos[i] = yPos[i + 1];
				yPos[i + 1] = j;
				cl++;
			}
			
			if (i == yPos.length-2 && cl >0) {
				i = -1;
				cl = 0;
			}
			
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		for (int i = 0; i < a; i++) {
			tree(xPos[i], yPos[i], defColor[i], g);
		}
//		pine(0, 0, g);

//		int temp[][] = placementArea();
//		for (int i = 0; i < 3; i++) {
//			System.out.println(temp[0][i]);
//		}
//		for (int i = 0; i < 3; i++) {
//			System.out.println(temp[1][i]);
//		}
	}

	public void tree(int x, int y, int tempColor, Graphics g) {

		// stump
		g.setColor(new Color(0x800000));
		g.fillRect(x - 7, y - 80, 15, 80);

		// leaves
		g.setColor(new Color(tempColor));
		g.fillOval(x - 35, y - 160, 71, 80);
	}

//	public int[][] placementArea(TreePanelFrame frame) {
//
//		int Winx = frame.getHeight();
//		int Winy = frame.getWidth();
//
////		int offx[] = null;
////		int offy[] = null;
//
//		int offset[][] = new int[2][3];
//
//		for (int i = 0; i < 3; i++) {
//			offset[0][i] = (Winx / 3) * i;
//		}
//
//		for (int i = 0; i < 3; i++) {
//			offset[1][i] = (Winy / 3) * i;
//		}
//
//		return offset;
//	}

//	public void pine(int x, int y, Graphics g) {
//
//		// Variables
//		int[] cGreen = { 0x7CFC00, 0x7FFF00, 0x32CD32, 0x00FF00, 0x228B22, 0x008000, 0x006400 };
//
//		g.setColor(new Color(0x800000));
//		g.fillRect(600, 475, 20, 80);
//
//		// leafs
//		for (int i = 0; i < 15; i++) {
//
//			for (int j = 0; j < i; j++) {
//				g.setColor(new Color(cGreen[i % 7]));
//				g.fillOval((605 - i * 5) + j * 10, 425 + i * 5, 10, 10);
//			}
//		}
//	}

}

public class TreePanelFrame extends JFrame {
	static final long serialVersionUID = 1L;
	TreePanel view = new TreePanel();

	public TreePanelFrame() {
		setTitle("TreePanelFrame");
		add(view);
		setSize(1200, 800);
		view.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				System.out.println("x: " + e.getX() + " y: " + e.getY());
			}
			
		});
//		view.addPropertyChangeListener((PropertyChangeListener) new WindowsBorders());
		
		view.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				view.init();
				view.repaint();
			}
		});
		
		view.init();
	}

	public static void main(String args[]) {
		TreePanelFrame frame = new TreePanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.setResizable(true);
		frame.setVisible(true);
	}
}
