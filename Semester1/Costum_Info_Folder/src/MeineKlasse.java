/*
 * Hallo zusammen
 * Hier werde ich euch versuchen Hilfestellung zu geben bez�glich Aufbau einer
 * Klasse und dazugeh�rige Elemente.
 */

// Das sind Imports
import java.awt.*;
import java.awt.Graphics;
/*
 * Imports kann man als Pakete anschauen, die deiner Klasse in seine Funktionalit�t erweitert.
 * Ein gutes Beispiel ist der java.awt.Graphics, der euch die M�glichkeit bietet Graphische
 * Funktionalit�t anzueignen, wie der g.draw... oder g.fill...
 */

/*
 * Das hier ist eine Klasse. Die Klasse ist das �usserste Frame oder Schale euers Programms.
 * Man kann mehrere Klassen haben die alle etwas verschiedenes machen, doch f�r uns reicht
 * eine Klasse.
 */
public class MeineKlasse {
	
	// Attribute der Klasse, "globale Attribute"
	
	byte b = 0; // 8bit
	short s; // 16bit
	int x = 5; // 32bit
	long l; // 64bit
	float f; // Signum, 8bit Exponent, 23bit Matrize
	double d; // Signum, 11bit Exponent, 52bit Matrize
/*
 * globale Attribute k�nnen immer in der dazugeh�rige Klasse verwendet werden,
 * egal ob hier, in einer Methode oder Funktion.
 * 
 * Beim Kompilieren werden die Attribute zuerst initialisiert, dann die Konstruktoren
 * und am Ende die Methoden.
 */
	
// Attribute definieren und initialisieren ist nicht das gleiche:
	
	// das ist eine Definition
	int hallo;
	// Man sagt den Kompeiler das hallo einen int ist, aber keinen Wert besitzt
	
	// so definiert und initialisiert man einen Attribut
	int hi = 0;
	// Man hat jetzt zus�tlich hi eine Wert gegeben und ihn korekt initialisiert
	
/*
 * man kann auch Attribute sp�ter initialisieren. Ich habe Variable hallo
 * unten im init() initialisiert
 * 
 * Wichtig: man sollte keine uninitialisierte Variabeln einfach so gebrauchen,
 * da der Wert undefiniert ist. Das kann zu relativ interesante Nebeneffekte f�hren.
 */
	
/*
 * Das ist eine Methode
 * Methoden bestehen aus einer Eigenschaft, R�ckgabe, Namen und Eingabe
 * 
 * Eigenschaft:	private, Methode kann nur �ber einer anderen Methode aufgerufen werden
 * 				public, Methode kann immer aufgerufen werden
 * 
 * R�ckgabe:	void, keine R�ckgabe; man hat kein return "Wert" in der Methode
 * 				int, gibt einen Wert mit Datentyp int zur�ck
 * 				float, gibt einen Wert mit Datentyp float zur�ck
 * 				und so weiter, ihr versteht. Funktioniert auch als array (int[])
 * 
 * Name:		ist klar, man sollte zumindest den Namen so w�hlen das man grob weiss was es macht
 * 
 * Eingabe:		muss nicht immer eine haben (init()), beinhaltet Datentyp und wie die Variable
 * 				in der Methode heissen soll
 */
	public void MeineMethode (byte b2, int x) {
		byte bobi = b2;
		int y;
		System.out.println("bobi ist: " + bobi);
		
		// hier das x ist die Variable die von der Eingabe reinkopiert wurde
		// es wird nur die lokaler Variable x �berschrieben und nicht die globale
		// Variable x
		x = x +5;
		// this.x ist die globale Variable x
		y = this.x;
		
		// wenn wir dies in der Konsole ausgeben:
		System.out.println("x ist " + x);
		System.out.println("y ist " + y);
		
		// dann sollten stehen:	x ist 10
		// 						y ist 5
		
		// Man kann auch die globale Variable �berschreiben, ist aber nicht zu empfehlen
		this.x = x + y;
		
		this.l = 10;
		
	}
/*
 * Hier endet die Methode. Das heisst alle lokalen Variablen "sterben"
 * (bobi, b2, x, y) und k�nnen nicht ausserhalb der Methode gebraucht werden.
 * 
 * Dies gillt f�r alle Methoden, Funktionen und Klassen:
 * Attribute leben nur zwischen den geschweiften Klammern ihrer Definitions Ortes
 */
	
//	bobi = 3;	// geht nicht gibt einen Fehler zur�ck
	
	public void init() {
		
		// hallo wird initialisiert
		hallo = 0;
		
		MeineMethode(b, x);
		
		System.out.println(l);
	}
	
	// main wird hier als "entrypoint" Startpunkt gesehen.
	// Ab hier l�uft das Programm ab
	// Ich habe im Moment nichts drinnen, also l�uft auch nichts
	
	public static void main(String[] args) {
		
		
	}
	
}
