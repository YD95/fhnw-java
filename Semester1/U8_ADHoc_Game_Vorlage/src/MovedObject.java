import java.awt.Image;

public class MovedObject extends PictureObject {

	protected double vx;
	protected double vy;

	public MovedObject(int x, int y, Image pic) {
		super(x, y, pic);

	}

	public MovedObject(int x, int y, double vx, double vy, Image pic) {
		super(x, y, pic);

	}

	public void update() {

	}

}
