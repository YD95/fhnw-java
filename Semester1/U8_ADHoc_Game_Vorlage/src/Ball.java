import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

public class Ball extends MovedObject {

	protected Rectangle rect;

	public Ball(int x, int y, double vx, double vy, Image pic) {
		super(x, y, vx, vy, pic);

	}

	public void show(Graphics g) {
		g.drawRect(x, y, width, height);
		g.drawImage(pic, x, y, width, height, null);

	}

	public void setRectangle(Rectangle rect) {

	}

	public void update() {

	}

}
