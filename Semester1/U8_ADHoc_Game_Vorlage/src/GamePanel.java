import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GamePanel implements KeyListener, SimpleTimerListener {

	private boolean stop;
	
	private SimpleTimer timer;
	private Ball ball;
	private Bat bat;
	
	private Image ballPic = Utility.loadResourceImage("ball.png");
	private Image batPic = Utility.loadResourceImage("schlaeger.png");


	public GamePanel() {
		
		timer = new SimpleTimer(50);
		ball = new Ball(500, 400, 10, 10, ballPic);
		bat = new Bat(800, 350, batPic);
		
//		ball.show(null);
//		bat.show(null);

	}

	@Override
	public void timerAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public void paintComponent(Graphics g) {

	}

	public void setStop(boolean stop) {

	}

	protected int random(int from, int till) {
		return till;

	}

}
