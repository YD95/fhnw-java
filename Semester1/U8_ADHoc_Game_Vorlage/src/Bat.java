import java.awt.Image;

public class Bat extends PictureObject {

	public Bat(int x, int y, Image pic) {
		super(x, y, pic);
		
		setPosition(x, y);

	}

	public void setPosition(int x, int y) {
		
		this.x = x;
		this.y = y;

	}

	public boolean testCollision(MovedObject movedObject) {
		return false;

	}

}
