import java.awt.Graphics;
import java.awt.Image;

public class PictureObject {

	protected int x;
	protected int y;
	protected int width;
	protected int height;

	protected Image pic;

	public PictureObject(int x, int y, Image pic) {
		
		this.x = x;
		this.y = y;
		this.width = pic.getWidth(null);
		this.height = pic.getHeight(null);
		
		this.pic = pic;

	}

	public void show(Graphics g) {
		
		g.drawImage(pic, x, y, width, height, null);

	}

}
