import java.awt.Color;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class PongApp extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image icon = Utility.loadResourceImage("ball.png");
	private TopViewPanel view;

	public PongApp() {
		view = new TopViewPanel();
		setTitle("Pong-Game");
		setIconImage(icon);
		view.setBackground(new Color(224, 224, 224));
		add(view);
		setSize(1000, 800);
		setResizable(false);
		setVisible(true);
	}

	public static void main(String args[]) {
		PongApp frame = new PongApp();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
}
