import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import javax.swing.*;

class TreePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private Color stammBraun = new Color(160, 70, 0);
	private Color blattGruen = new Color(40, 160, 0);
	private int[] xPosition = new int[50];
	private int[] yPosition = new int[50];

	public void init() {
		for (int i = 0; i < 50; i++) {
			xPosition[i] = (int) ((Math.random() - 0.5) * 300) + 250;
			yPosition[i] = (int) ((Math.random() - 0.5) * 200) + 200;
		}
		yPosition = bubbleSort(yPosition);

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < xPosition.length; i++) {
			baum(g, xPosition[i], yPosition[i]);
		}
	}

	/**
	 * Array sortieren:
	 * 
	 * Beginnend mit dem 0ten Element werden immer zwei nebeneinander liegende
	 * Elemente verglichen. Fall das nachfolgende kleiner ist als das vorg�ngige
	 * werden die beiden getauscht. Dieser Vorgang wird wiederholt bis nicht mehr
	 * getauscht werden kann.
	 * 
	 * <pre>
	 * 
	 * boolean getauscht deklarieren und true setzen.
	 * - Solange getauscht:
	 *   - getauscht gleich false setzen.
	 *   - F�r i gleich Null bis i kleiner (Anzahl Elemente minus 1):
	 *     - Falls i-tes Element gr�sser als (i+1)-tes Element:
	 *         - i-tes Element int tmp schreiben.
	 *         - (i+1)-tes Element in i-tes schreiben.
	 *         - tmp in (i+1)-tes schreiben.
	 *         - boolean getauscht auf true setzen.
	 * </pre>
	 * 
	 * 
	 * @param a
	 * @return
	 */
	public int[] bubbleSort(int[] a) {
		boolean getauscht = true;
		while (getauscht) {
			getauscht = false;
			for (int i = 0; i < a.length - 1; i++) {
				if (a[i] > a[i + 1]) {
					int tmp = a[i];
					a[i] = a[i + 1];
					a[i + 1] = tmp;
					getauscht = true;
				}
			}
		}
		return a;
	}

	public void baum(Graphics g, int x, int y) {
		g.setColor(stammBraun);
		g.fillRect(x - 10, y - 60, 20, 60);
		g.setColor(blattGruen);

		g.fillOval(x - 40, y - 100, 50, 60);
		g.fillOval(x - 20, y - 100, 50, 60);
	}

}

public class TreePanelFrame extends JFrame {
	static final long serialVersionUID = 1L;
	TreePanel view = new TreePanel();

	public TreePanelFrame() {
		setTitle("TreePanelFrame");
		add(view);
		setSize(600, 400);
		view.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				System.out.println("x: " + e.getX() + " y: " + e.getY());
			}
		});
		view.init();
	}

	public static void main(String args[]) {
		TreePanelFrame frame = new TreePanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.setResizable(true);
		frame.setVisible(true);
	}
}
