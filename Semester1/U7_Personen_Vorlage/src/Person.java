import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.sound.sampled.Clip;
import javax.swing.JPanel;

public class Person extends JPanel implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String def = "N/A";

	private String surname = def;
	private String name = def;
	private boolean paintFlag = false;
	private Image pic;
	private Clip audio;

	public Person(String surname, String name, String pic, String audio) {
		this.surname = surname;
		this.name = name;
		this.pic = Utility.loadResourceImage(pic);
		this.audio = Utility.loadAudioClip(audio);
		addMouseListener(this);

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (paintFlag) {
			g.setColor(Color.pink);
		} else {
			g.setColor(Color.lightGray);
		}
		g.fillRect(5, 5, 205, 340);
		g.setColor(Color.black);
		g.drawString("Surname: " + surname, 20, 20);
		g.drawString("Name: " + name, 20, 40);
		g.drawImage(pic, 20, 50, null);
		g.drawRect(5, 5, 205, 340);

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

		paintFlag = true;
		audio.setFramePosition(0);
		audio.start();
		repaint();

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

		paintFlag = false;
		repaint();

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
