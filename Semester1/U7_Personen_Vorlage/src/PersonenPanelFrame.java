import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;

class PersonenPanel extends JPanel {

	public PersonenPanel() {
		setLayout(null);

		Person person1 = new Person("Aniston", "Jennifer", "JenniferAniston.jpg", "catmeow.wav");
		Person person2 = new Person("Matt", "Leblanc", "MattLeblanc.jpg", "lion.wav");
		Person person3 = new Person("Diaz", "Cameron", "CameronDiaz.jpg", "catmeow.wav");
		Person person4 = new Person("Levy", "William", "WilliamLevy.jpg", "lion.wav");

		add(person1).setBounds(15, 15, 275, 355);
		add(person2).setBounds(300, 15, 275, 355);
		add(person3).setBounds(15, 370, 275, 355);
		add(person4).setBounds(300, 370, 275, 355);

	}

}

public class PersonenPanelFrame extends JFrame {
	private PersonenPanel view;

	public PersonenPanelFrame() {
		view = new PersonenPanel();
		add(view);
		setSize(600, 800);
		setTitle("Personen - Panel");
		setIconImage(Utility.loadResourceImage("fhnw_logo.png"));
		setResizable(true);
		setVisible(true);
	}

	public static void main(String args[]) {
		PersonenPanelFrame frame = new PersonenPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
}