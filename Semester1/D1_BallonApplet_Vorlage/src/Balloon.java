import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class Balloon {
	private Color color = Color.red;
	private int x, y, d;
	private Image pic;

	public Balloon(int x, int y, int d) {
		this.x = x;
		this.y = y;
		this.d = d;
		pic = Utility.loadResourceImage("Image_" + (int) ((Math.random() * 8.0) + 1) + ".png");
		pic = pic.getScaledInstance((int) (d + Math.random() * 40), -1, Image.SCALE_DEFAULT);
		
	}

	public void show(Graphics g) {
		if (y < 25)
			return;
		
		g.setColor(color);
//		g.fillOval(x - d / 2, y - d / 2, d, d);
		g.drawImage(pic, x - d / 2, y - d / 2, null);

	}

	public void update() {
		y -= 1;
	}
	
}
