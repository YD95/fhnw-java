import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class BallonPanel extends JPanel implements ActionListener, SimpleTimerListener {
	private JButton btWachsen = new JButton("Wachsen");
	private JButton btSchrumpfen = new JButton("Schrumpfen");
	private Balloon myBalloon = new Balloon(150, 200, 100);
	private Balloon yourBalloon = new Balloon(400, 200, 100);
	private Balloon[] balloon = new Balloon[24];

	public void init() {
		setLayout(null);
		btWachsen.addActionListener(this);
		btSchrumpfen.addActionListener(this);
		add(btWachsen).setBounds(25, 525, 250, 25);
		add(btSchrumpfen).setBounds(300, 525, 250, 25);

		for (int i = 0; i < balloon.length; i++) {
			balloon[i] = new Balloon((int) (Math.random() * 600), (int) (400 + 200 * Math.random()), 75);
		}

		new SimpleTimer(50, this);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		myBalloon.show(g);
		yourBalloon.show(g);
		for (int i = 0; i < balloon.length; i++) {
			balloon[i].show(g);
		}
	}

	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void timerAction() {
		// TODO Auto-generated method stub
		myBalloon.update();
		yourBalloon.update();
		for (int i = 0; i < balloon.length; i++) {
			balloon[i].update();
		}
		repaint();

	}
}

public class BallonPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static BallonPanel view = new BallonPanel();

	public static void main(String args[]) {
		BallonPanelFrame frame = new BallonPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		view.init();
		frame.add(view);
		frame.setSize(600, 600);
		frame.setTitle("FHNW Ballon-Panel");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}
