import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

class SliderPanel extends JPanel implements ChangeListener {
	private static final long serialVersionUID = 1L;
	private JSlider schieber;

	public void init() {
		setLayout(null);
		schieber = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
		add(schieber).setBounds(20, 20, 300, 20);
		schieber.addChangeListener(this);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		System.out.println("Schieber - Wert: " + schieber.getValue());
	}
}

public class SliderDemo extends Frame {
	private static final long serialVersionUID = 1L;

	public static void main(String args[]) {
		SliderDemo frame = new SliderDemo();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		SliderPanel view = new SliderPanel();
		frame.add(view);
		frame.setSize(400, 500);
		view.init();
		frame.setResizable(false);
		frame.setVisible(true);
	}
}