import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

class MouseDemoPanel extends JPanel implements MouseListener {
	private static final long serialVersionUID = 1L;

	public void init() {
		addMouseListener(this);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println("mouseClicked()");
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		System.out.println("mouseEntered()");
	}

	@Override
	public void mouseExited(MouseEvent e) {
		System.out.println("mouseExited()");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		System.out.println("mousePressed()");
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("mouseReleased()");
	}
}

public class MouseDemo extends JFrame {
	private static final long serialVersionUID = 1L;

	public static void main(String args[]) {
		MouseDemo frame = new MouseDemo();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		MouseDemoPanel view = new MouseDemoPanel();
		frame.add(view);
		frame.setSize(800, 800);
		view.init();
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
