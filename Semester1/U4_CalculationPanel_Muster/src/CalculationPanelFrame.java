import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class CalculationPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private static final int HORIZONTAL = 0, VERTIKAL = 1;
	private JTextField tfR1 = new JTextField("10.0");
	private JTextField tfR2 = new JTextField("10.0");
	private JTextField tfR3 = new JTextField("10.0");
	private JTextField tfR4 = new JTextField("10.0");
	private JTextField tfU = new JTextField("10.0");
	private JButton btBerechnen = new JButton("Berechnen");
	private JLabel lbResultat = new JLabel("Resultat");
	private double uR2;

	public void init() {
		setLayout(null);
//		if (getFont() != null) {
//			setFont(getFont().deriveFont(12.0f));
//		}
		int y = 10;
		add(new Label("R1: ")).setBounds(10, y, 40, 20);
		add(tfR1).setBounds(60, y, 240, 20);
		add(new Label(" Ohm")).setBounds(310, y, 40, 20);
		y += 30;
		add(new Label("R2: ")).setBounds(10, y, 40, 20);
		add(tfR2).setBounds(60, y, 240, 20);
		add(new Label(" Ohm")).setBounds(310, y, 40, 20);
		y += 30;
		add(new Label("R3: ")).setBounds(10, y, 40, 20);
		add(tfR3).setBounds(60, y, 240, 20);
		add(new Label(" Ohm")).setBounds(310, y, 40, 20);
		y += 30;
		add(new Label("R4: ")).setBounds(10, y, 40, 20);
		add(tfR4).setBounds(60, y, 240, 20);
		add(new Label(" Ohm")).setBounds(310, y, 40, 20);
		y += 30;
		add(new Label("U: ")).setBounds(10, y, 40, 20);
		add(tfU).setBounds(60, y, 240, 20);
		add(new Label(" Volt")).setBounds(310, y, 40, 20);
		y += 30;
		add(btBerechnen).setBounds(60, y, 240, 20);
		
		add(lbResultat).setBounds(60, 460, 240, 20);
		
		btBerechnen.addActionListener(this);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		zeichneWiderstand(g, VERTIKAL, "R1", 200, 220);
		zeichneWiderstand(g, VERTIKAL, "R2", 200, 300);
		g.drawLine(160, 380, 240, 380);
		zeichneWiderstand(g, VERTIKAL, "R3", 160, 380);
		zeichneWiderstand(g, VERTIKAL, "R4", 240, 380);
		g.drawLine(150, 460, 170, 460);
		g.drawLine(230, 460, 250, 460);
//		g.drawString("Spannung �ber R2: " + ((int) (100 * uR2)) / 100.0 + " Volt", 120, 500);
	}

	public void zeichneWiderstand(Graphics g, int ausrichtung, String txt, int x, int y) {
		if (ausrichtung == HORIZONTAL) {
			g.drawLine(x, y, x + 20, y);
			g.drawRect(x + 20, y - 10, 40, 20);
			g.drawLine(x + 60, y, x + 80, y);
			g.drawString(txt, x + 35, y - 20);
		}
		if (ausrichtung == VERTIKAL) {
			g.drawLine(x, y, x, y + 20);
			g.drawRect(x - 10, y + 20, 20, 40);
			g.drawLine(x, y + 60, x, y + 80);
			g.drawString(txt, x + 20, y + 45);
		}
	}

	public void actionPerformed(ActionEvent e) {
		double r1 = Double.parseDouble(tfR1.getText());
		double r2 = Double.parseDouble(tfR2.getText());
		double r3 = Double.parseDouble(tfR3.getText());
		double r4 = Double.parseDouble(tfR4.getText());
		double u = Double.parseDouble(tfU.getText());

		uR2 = berechneUx(r1, r2, r3, r4, u);

		lbResultat.setText("Resultat: "+uR2);
		
		repaint();
	}

	private double berechneUx(double r1, double r2, double r3, double r4, double u) {
		return u / (r1 + r2 + (r3 * r4) / (r3 + r4)) * r2;
	}
}

public class CalculationPanelFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private static Image icon = Utility.loadResourceImage("heart.png");

	public static void main(String args[]) {
		CalculationPanelFrame frame = new CalculationPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		CalculationPanel view = new CalculationPanel();
		view.init();
//		view.addMouseMotionListener(new MouseMotionAdapter() {
//			public void mouseMoved(MouseEvent e) {
//				System.out.println("x: " + e.getX() + " y: " + e.getY());
//			}
//		});
		frame.add(view);
		frame.setSize(400, 600);
		frame.setIconImage(icon);
		frame.setTitle("|FHNW|EIT|OOP|Calculation-Panel|");
		frame.setResizable(true);
		frame.setVisible(true);
	}
}