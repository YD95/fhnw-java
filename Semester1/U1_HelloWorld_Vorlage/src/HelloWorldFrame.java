import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class HelloWorldPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public void init() {

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		//position origin top left for all
		g.setColor(Color.GREEN);
		g.fillOval(100, 100, 450, 450);

		g.setColor(new Color(0x00));
		g.drawRect(100, 100, 300, 350);

	}
}

public class HelloWorldFrame extends JFrame {
	static final long serialVersionUID = 1L;
	HelloWorldPanel view = new HelloWorldPanel();

	public HelloWorldFrame() {
		setTitle("HelloWorldFrame");
		add(view);
		setSize(800, 800);
		view.init();
	}

	public static void main(String args[]) {
		HelloWorldFrame frame = new HelloWorldFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
		frame.setResizable(true);
		frame.setVisible(true);
	}
}
